﻿using System.Collections.Generic;
using System.Linq;
using Mutagen.Bethesda.FormKeys.SkyrimSE;
using Mutagen.Bethesda.Json;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Skyrim;
using Mutagen.Bethesda.WPF.Reflection.Attributes;
using MutagenLibrary.WPF.Json.Editor;
using MutagenLibrary.WPF.Json.Presets;
using Newtonsoft.Json;
using Noggog;
namespace MutagenTools.EnchantmentGenerator.Settings; 

public class EnchantmentJsonEditor : JsonEditor<List<EnchantmentType>> {}
public class EnchantmentsConfigurator : PresetsConfigurator<List<EnchantmentType>, EnchantmentsPreset, EnchantmentJsonEditor> {
    public override string Header { get; set; } = "Enchantments";
}

public class EnchantmentsPreset : PresetManager<List<EnchantmentType>, EnchantmentsPreset> {
    protected override string PresetsDirectoryName => "Enchantments";

    protected override List<EnchantmentType> DefaultData => new() {
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchWeaponAbsorbHealth02, 2, string.Empty, "of Absorption"),
                new(Skyrim.ObjectEffect.EnchWeaponAbsorbHealth03, 3, string.Empty, "of Consuming"),
                new(Skyrim.ObjectEffect.EnchWeaponAbsorbHealth04, 4, string.Empty, "of Devouring"),
                new(Skyrim.ObjectEffect.EnchWeaponAbsorbHealth05, 5, string.Empty, "of Leeching"),
                new(Skyrim.ObjectEffect.EnchWeaponAbsorbHealth06, 6, string.Empty, "of the Vampire"),
            },
            "AbsorbH",
            new List<BlacklistedKeywords> {
                new(new List<BlacklistedKeyword> { new(Skyrim.Keyword.WeapTypeBow) }),
            }
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchWeaponAbsorbMagicka02, 2, string.Empty, "of Siphoning"),
                new(Skyrim.ObjectEffect.EnchWeaponAbsorbMagicka03, 3, string.Empty, "of Harrowing"),
                new(Skyrim.ObjectEffect.EnchWeaponAbsorbMagicka04, 4, string.Empty, "of Winnowing"),
                new(Skyrim.ObjectEffect.EnchWeaponAbsorbMagicka05, 5, string.Empty, "of Evoking"),
                new(Skyrim.ObjectEffect.EnchWeaponAbsorbMagicka06, 6, string.Empty, "of the Sorcerer"),
            },
            "AbsorbM",
            new List<BlacklistedKeywords> {
                new(new List<BlacklistedKeyword> { new(Skyrim.Keyword.WeapTypeBow) }),
            }
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchWeaponAbsorbStamina02, 2, string.Empty, "of Gleaning"),
                new(Skyrim.ObjectEffect.EnchWeaponAbsorbStamina03, 3, string.Empty, "of Reaping"),
                new(Skyrim.ObjectEffect.EnchWeaponAbsorbStamina04, 4, string.Empty, "of Harvesting"),
                new(Skyrim.ObjectEffect.EnchWeaponAbsorbStamina05, 5, string.Empty, "of Garnering"),
                new(Skyrim.ObjectEffect.EnchWeaponAbsorbStamina06, 6, string.Empty, "of Subsuming"),
            },
            "AbsorbS",
            new List<BlacklistedKeywords> {
                new(new List<BlacklistedKeyword> { new(Skyrim.Keyword.WeapTypeBow) }),
            }
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchWeaponBanish04, 4, string.Empty, "of Banishing"),
                new(Skyrim.ObjectEffect.EnchWeaponBanish05, 5, string.Empty, "of Expelling"),
                new(Skyrim.ObjectEffect.EnchWeaponBanish06, 6, string.Empty, "of Annihilating"),
            },
            "Banish"
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchWeaponFear01, 1, string.Empty, "of Dismay"),
                new(Skyrim.ObjectEffect.EnchWeaponFear02, 2, string.Empty, "of Cowardice"),
                new(Skyrim.ObjectEffect.EnchWeaponFear03, 3, string.Empty, "of Fear"),
                new(Skyrim.ObjectEffect.EnchWeaponFear04, 4, string.Empty, "of Despair"),
                new(Skyrim.ObjectEffect.EnchWeaponFear05, 5, string.Empty, "of Dread"),
                new(Skyrim.ObjectEffect.EnchWeaponFear06, 6, string.Empty, "of Terror"),
            },
            "Fear",
            new List<BlacklistedKeywords> {
                new(new List<BlacklistedKeyword> { new(Dawnguard.Keyword.WeapDwarvenCrossbow) }),
            }
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchWeaponFireDamage01, 1, string.Empty, "of Embers"),
                new(Skyrim.ObjectEffect.EnchWeaponFireDamage02, 2, string.Empty, "of Burning"),
                new(Skyrim.ObjectEffect.EnchWeaponFireDamage03, 3, string.Empty, "of Scorching"),
                new(Skyrim.ObjectEffect.EnchWeaponFireDamage04, 4, string.Empty, "of Fire"),
                new(Skyrim.ObjectEffect.EnchWeaponFireDamage05, 5, string.Empty, "of the Blaze"),
                new(Skyrim.ObjectEffect.EnchWeaponFireDamage06, 6, string.Empty, "of the Inferno"),
            },
            "Fire",
            new List<BlacklistedKeywords> {
                new(new List<BlacklistedKeyword> { new(Dragonborn.Keyword.DLC2WeaponMaterialStalhrim) }),
            }
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchWeaponFrostDamage01, 1, string.Empty, "of Cold"),
                new(Skyrim.ObjectEffect.EnchWeaponFrostDamage02, 2, string.Empty, "of Frost"),
                new(Skyrim.ObjectEffect.EnchWeaponFrostDamage03, 3, string.Empty, "of Ice"),
                new(Skyrim.ObjectEffect.EnchWeaponFrostDamage04, 4, string.Empty, "of Freezing"),
                new(Skyrim.ObjectEffect.EnchWeaponFrostDamage05, 5, string.Empty, "of Blizzards"),
                new(Skyrim.ObjectEffect.EnchWeaponFrostDamage06, 6, string.Empty, "of Winter"),
            },
            "Frost"
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchWeaponShockDamage01, 1, string.Empty, "of Sparks"),
                new(Skyrim.ObjectEffect.EnchWeaponShockDamage02, 2, string.Empty, "of Arcing"),
                new(Skyrim.ObjectEffect.EnchWeaponShockDamage03, 3, string.Empty, "of Shocks"),
                new(Skyrim.ObjectEffect.EnchWeaponShockDamage04, 4, string.Empty, "of Thunderbolts"),
                new(Skyrim.ObjectEffect.EnchWeaponShockDamage05, 5, string.Empty, "of Lightning"),
                new(Skyrim.ObjectEffect.EnchWeaponShockDamage06, 6, string.Empty, "of Storms"),
            },
            "Shock"
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchWeaponStaminaDamage01, 1, string.Empty, "of Fatigue"),
                new(Skyrim.ObjectEffect.EnchWeaponStaminaDamage02, 2, string.Empty, "of Weariness"),
                new(Skyrim.ObjectEffect.EnchWeaponStaminaDamage03, 3, string.Empty, "of Torpor"),
                new(Skyrim.ObjectEffect.EnchWeaponStaminaDamage04, 4, string.Empty, "of Debilitation"),
                new(Skyrim.ObjectEffect.EnchWeaponStaminaDamage05, 5, string.Empty, "of Lethargy"),
                new(Skyrim.ObjectEffect.EnchWeaponStaminaDamage06, 6, string.Empty, "of Exhaustion"),
            },
            "Stamina"
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchWeaponMagickaDamage01, 1, string.Empty, "of Sapping"),
                new(Skyrim.ObjectEffect.EnchWeaponMagickaDamage02, 2, string.Empty, "of Draining"),
                new(Skyrim.ObjectEffect.EnchWeaponMagickaDamage03, 3, string.Empty, "of Diminishing"),
                new(Skyrim.ObjectEffect.EnchWeaponMagickaDamage04, 4, string.Empty, "of Depleting"),
                new(Skyrim.ObjectEffect.EnchWeaponMagickaDamage05, 5, string.Empty, "of Enervating"),
                new(Skyrim.ObjectEffect.EnchWeaponMagickaDamage06, 6, string.Empty, "of Nullifying"),
            },
            "Magicka"
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchWeaponTurnUndead01, 1, "Blessed"),
                new(Skyrim.ObjectEffect.EnchWeaponTurnUndead02, 2, "Sanctified"),
                new(Skyrim.ObjectEffect.EnchWeaponTurnUndead03, 3, "Reverent"),
                new(Skyrim.ObjectEffect.EnchWeaponTurnUndead04, 4, "Hallowed"),
                new(Skyrim.ObjectEffect.EnchWeaponTurnUndead05, 5, "Virtuous"),
                new(Skyrim.ObjectEffect.EnchWeaponTurnUndead06, 6, "Holy"),
            },
            "Turn"
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchWeaponParalysis04, 4, string.Empty, "of Stunning"),
                new(Skyrim.ObjectEffect.EnchWeaponParalysis05, 5, string.Empty, "of Immobilizing"),
                new(Skyrim.ObjectEffect.EnchWeaponParalysis06, 6, string.Empty, "of Petrifying"),
            },
            "Paralysis"
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchWeaponSoulTrap01, 1, string.Empty, "of Souls"),
                new(Skyrim.ObjectEffect.EnchWeaponSoulTrap02, 2, string.Empty, "of Soul Snares"),
                new(Skyrim.ObjectEffect.EnchWeaponSoulTrap03, 3, string.Empty, "of Binding"),
                new(Skyrim.ObjectEffect.EnchWeaponSoulTrap04, 4, string.Empty, "of Animus"),
                new(Skyrim.ObjectEffect.EnchWeaponSoulTrap05, 5, string.Empty, "of Malediction"),
                new(Skyrim.ObjectEffect.EnchWeaponSoulTrap06, 6, string.Empty, "of Damnation"),
            },
            "SoulTrap"
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchArmorFortifyAlchemy01, 1, string.Empty, "of Minor Alchemy"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyAlchemy02, 2, string.Empty, "of Alchemy"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyAlchemy03, 3, string.Empty, "of Major Alchemy"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyAlchemy04, 4, string.Empty, "of Eminent Alchemy"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyAlchemy05, 5, string.Empty, "of Extreme Alchemy"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyAlchemy06, 6, string.Empty, "of Peerless Alchemy"),
            },
            "Alchemy"
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchArmorFortifyAlteration01, 1, string.Empty, "of Minor Alteration"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyAlteration02, 2, string.Empty, "of Alteration"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyAlteration03, 3, string.Empty, "of Major Alteration"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyAlteration04, 4, string.Empty, "of Eminent Alteration"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyAlteration05, 5, string.Empty, "of Extreme Alteration"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyAlteration06, 6, string.Empty, "of Peerless Alteration"),
            },
            "Alteration"
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchArmorFortifyBlock01, 1, string.Empty, "of Minor Blocking"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyBlock02, 2, string.Empty, "of Blocking"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyBlock03, 3, string.Empty, "of Major Blocking"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyBlock04, 4, string.Empty, "of Eminent Blocking"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyBlock05, 5, string.Empty, "of Extreme Blocking"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyBlock06, 6, string.Empty, "of Peerless Blocking"),
            },
            "Block",
            new List<BlacklistedKeywords> {
                new(new List<BlacklistedKeyword> { new(Skyrim.Keyword.ArmorGauntlets) }),
            }
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchArmorFortifyConjuration01, 1, string.Empty, "of Minor Conjuration"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyConjuration02, 2, string.Empty, "of Conjuration"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyConjuration03, 3, string.Empty, "of Major Conjuration"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyConjuration04, 4, string.Empty, "of Eminent Conjuration"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyConjuration05, 5, string.Empty, "of Extreme Conjuration"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyConjuration06, 6, string.Empty, "of Peerless Conjuration"),
            },
            "Conjuration"
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchArmorFortifyDestruction01, 1, string.Empty, "of Minor Destruction"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyDestruction02, 2, string.Empty, "of Destruction"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyDestruction03, 3, string.Empty, "of Major Destruction"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyDestruction04, 4, string.Empty, "of Eminent Destruction"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyDestruction05, 5, string.Empty, "of Extreme Destruction"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyDestruction06, 6, string.Empty, "of Peerless Destruction"),
            },
            "Destruction"
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchArmorFortifyHeavyArmor01, 1, string.Empty, "of the Minor Knight"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyHeavyArmor02, 2, string.Empty, "of the Knight"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyHeavyArmor03, 3, string.Empty, "of the Major Knight"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyHeavyArmor04, 4, string.Empty, "of the Eminent Knight"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyHeavyArmor05, 5, string.Empty, "of the Noble Knight"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyHeavyArmor06, 6, string.Empty, "of the Peerless Knight"),
            },
            "HeavyArmor",
            new List<BlacklistedKeywords> {
                new(new List<BlacklistedKeyword> { new(Skyrim.Keyword.ArmorCuirass), new(Skyrim.Keyword.ArmorLight) }),
                new(new List<BlacklistedKeyword> { new(Skyrim.Keyword.ArmorGauntlets) }),
            }
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchArmorFortifyIllusion01, 1, string.Empty, "of Minor Illusion"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyIllusion02, 2, string.Empty, "of Illusion"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyIllusion03, 3, string.Empty, "of Major Illusion"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyIllusion04, 4, string.Empty, "of Eminent Illusion"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyIllusion05, 5, string.Empty, "of Extreme Illusion"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyIllusion06, 6, string.Empty, "of Peerless Illusion"),
            },
            "Illusion"
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchArmorFortifyLightArmor01, 1, string.Empty, "of the Minor Squire"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyLightArmor02, 2, string.Empty, "of the Squire"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyLightArmor03, 3, string.Empty, "of the Major Squire"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyLightArmor04, 4, string.Empty, "of the Eminent Squire"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyLightArmor05, 5, string.Empty, "of the Noble Knight"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyLightArmor06, 6, string.Empty, "of the Peerless Knight"),
            },
            "LightArmor",
            new List<BlacklistedKeywords> {
                new(new List<BlacklistedKeyword> { new(Skyrim.Keyword.ArmorCuirass), new(Skyrim.Keyword.ArmorHeavy) }),
                new(new List<BlacklistedKeyword> { new(Skyrim.Keyword.ArmorGauntlets) }),
            }
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchArmorFortifyLockpicking01, 1, string.Empty, "of Minor Lockpicking"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyLockpicking02, 2, string.Empty, "of Lockpicking"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyLockpicking03, 3, string.Empty, "of Major Lockpicking"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyLockpicking04, 4, string.Empty, "of Eminent Lockpicking"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyLockpicking05, 5, string.Empty, "of Extreme Lockpicking"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyLockpicking06, 6, string.Empty, "of Peerless Lockpicking"),
            },
            "Lockpicking",
            new List<BlacklistedKeywords> {
                new(new List<BlacklistedKeyword> { new(Skyrim.Keyword.ArmorGauntlets), new(Skyrim.Keyword.ArmorHeavy) }),
                new(new List<BlacklistedKeyword> { new(Skyrim.Keyword.ArmorHelmet) }),
            }
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchArmorFortifyMarksman01, 1, string.Empty, "of Minor Archery"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyMarksman02, 2, string.Empty, "of Archery"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyMarksman03, 3, string.Empty, "of Major Archery"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyMarksman04, 4, string.Empty, "of Eminent Archery"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyMarksman05, 5, string.Empty, "of Extreme Archery"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyMarksman06, 6, string.Empty, "of Peerless Archery"),
            },
            "Marksman"
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchArmorFortifyOneHanded01, 1, string.Empty, "of Minor Wielding"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyOneHanded02, 2, string.Empty, "of Wielding"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyOneHanded03, 3, string.Empty, "of Major Wielding"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyOneHanded04, 4, string.Empty, "of Eminent Wielding"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyOneHanded05, 5, string.Empty, "of Extreme Wielding"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyOneHanded06, 6, string.Empty, "of Peerless Wielding"),
            },
            "OneHanded",
            new List<BlacklistedKeywords> {
                new(new List<BlacklistedKeyword> { new(Skyrim.Keyword.ArmorBoots) }),
            }
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchArmorFortifyPickpocket01, 1, string.Empty, "of Minor Deft Hands"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyPickpocket02, 2, string.Empty, "of Deft Hands"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyPickpocket03, 3, string.Empty, "of Major Deft Hands"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyPickpocket04, 4, string.Empty, "of Eminent Deft Hands"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyPickpocket05, 5, string.Empty, "of Extreme Deft Hands"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyPickpocket06, 6, string.Empty, "of Peerless Deft Hands"),
            },
            "Pickpocketing",
            new List<BlacklistedKeywords> {
                new(new List<BlacklistedKeyword> { new(Skyrim.Keyword.ArmorBoots) }),
                new(new List<BlacklistedKeyword> { new(Skyrim.Keyword.ArmorGauntlets), new(Skyrim.Keyword.ArmorHeavy) }),
            }
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchArmorFortifyRestoration01, 1, string.Empty, "of Minor Restoration"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyRestoration02, 2, string.Empty, "of Restoration"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyRestoration03, 3, string.Empty, "of Major Restoration"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyRestoration04, 4, string.Empty, "of Eminent Restoration"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyRestoration05, 5, string.Empty, "of Extreme Restoration"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyRestoration06, 6, string.Empty, "of Peerless Restoration"),
            },
            "Restoration"
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchArmorFortifySmithing01, 1, string.Empty, "of Minor Smithing"),
                new(Skyrim.ObjectEffect.EnchArmorFortifySmithing02, 2, string.Empty, "of Smithing"),
                new(Skyrim.ObjectEffect.EnchArmorFortifySmithing03, 3, string.Empty, "of Major Smithing"),
                new(Skyrim.ObjectEffect.EnchArmorFortifySmithing04, 4, string.Empty, "of Eminent Smithing"),
                new(Skyrim.ObjectEffect.EnchArmorFortifySmithing05, 5, string.Empty, "of Extreme Smithing"),
                new(Skyrim.ObjectEffect.EnchArmorFortifySmithing06, 6, string.Empty, "of Peerless Smithing"),
            },
            "Smithing",
            new List<BlacklistedKeywords> {
                new(new List<BlacklistedKeyword> { new(Skyrim.Keyword.ArmorCuirass) }),
            }
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchArmorFortifySneak01, 1, string.Empty, "of Minor Sneaking"),
                new(Skyrim.ObjectEffect.EnchArmorFortifySneak02, 2, string.Empty, "of Sneaking"),
                new(Skyrim.ObjectEffect.EnchArmorFortifySneak03, 3, string.Empty, "of Major Sneaking"),
                new(Skyrim.ObjectEffect.EnchArmorFortifySneak04, 4, string.Empty, "of Eminent Sneaking"),
                new(Skyrim.ObjectEffect.EnchArmorFortifySneak05, 5, string.Empty, "of Extreme Sneaking"),
                new(Skyrim.ObjectEffect.EnchArmorFortifySneak06, 6, string.Empty, "of Peerless Sneaking"),
            },
            "Sneak",
            new List<BlacklistedKeywords> {
                new(new List<BlacklistedKeyword> { new(Skyrim.Keyword.ArmorBoots), new(Skyrim.Keyword.ArmorHeavy) }),
                new(new List<BlacklistedKeyword> { new(Skyrim.Keyword.ArmorGauntlets) }),
            }
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchArmorFortifySpeechcraft01, 1, string.Empty, "of Minor Haggling"),
                new(Skyrim.ObjectEffect.EnchArmorFortifySpeechcraft02, 2, string.Empty, "of Haggling"),
                new(Skyrim.ObjectEffect.EnchArmorFortifySpeechcraft03, 3, string.Empty, "of Major Haggling"),
                new(Skyrim.ObjectEffect.EnchArmorFortifySpeechcraft04, 4, string.Empty, "of Eminent Haggling"),
                new(Skyrim.ObjectEffect.EnchArmorFortifySpeechcraft05, 5, string.Empty, "of Extreme Haggling"),
                new(Skyrim.ObjectEffect.EnchArmorFortifySpeechcraft06, 6, string.Empty, "of Peerless Haggling"),
            },
            "Speechcraft"
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchArmorFortifyTwoHanded01, 1, string.Empty, "of Minor Sure Grip"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyTwoHanded02, 2, string.Empty, "of Sure Grip"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyTwoHanded03, 3, string.Empty, "of Major Sure Grip"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyTwoHanded04, 4, string.Empty, "of Eminent Sure Grip"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyTwoHanded05, 5, string.Empty, "of Extreme Sure Grip"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyTwoHanded06, 6, string.Empty, "of Peerless Sure Grip"),
            },
            "TwoHanded",
            new List<BlacklistedKeywords> {
                new(new List<BlacklistedKeyword> { new(Skyrim.Keyword.ArmorBoots) }),
            }
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchArmorFortifyHealth01, 1, string.Empty, "of Minor Health"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyHealth02, 2, string.Empty, "of Health"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyHealth03, 3, string.Empty, "of Major Health"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyHealth04, 4, string.Empty, "of Eminent Health"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyHealth05, 5, string.Empty, "of Extreme Health"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyHealth06, 6, string.Empty, "of Peerless Health"),
            },
            "Health",
            new List<BlacklistedKeywords> {
                new(new List<BlacklistedKeyword> { new(Skyrim.Keyword.ArmorShield) }),
            }
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchArmorFortifyMagicka01, 1, string.Empty, "of Minor Magicka"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyMagicka02, 2, string.Empty, "of Magicka"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyMagicka03, 3, string.Empty, "of Major Magicka"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyMagicka04, 4, string.Empty, "of Eminent Magicka"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyMagicka05, 5, string.Empty, "of Extreme Magicka"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyMagicka06, 6, string.Empty, "of Peerless Magicka"),
            },
            "Magicka",
            new List<BlacklistedKeywords> {
                new(new List<BlacklistedKeyword> { new(Skyrim.Keyword.ArmorGauntlets) }),
            }
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchArmorFortifyStamina01, 1, string.Empty, "of Minor Stamina"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyStamina02, 2, string.Empty, "of Stamina"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyStamina03, 3, string.Empty, "of Major Stamina"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyStamina04, 4, string.Empty, "of Eminent Stamina"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyStamina05, 5, string.Empty, "of Extreme Stamina"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyStamina06, 6, string.Empty, "of Peerless Stamina"),
            },
            "Stamina",
            new List<BlacklistedKeywords> {
                new(new List<BlacklistedKeyword> { new(Skyrim.Keyword.ArmorCuirass) }),
            }
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchArmorFortifyHealRate03, 3, string.Empty, "of Remedy"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyHealRate04, 4, string.Empty, "of Mending"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyHealRate05, 5, string.Empty, "of Regeneration"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyHealRate06, 6, string.Empty, "of Revival"),
            },
            "HealRate"
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchArmorFortifyMagickaRate03, 3, string.Empty, "of Recharging"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyMagickaRate04, 4, string.Empty, "of Replenishing"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyMagickaRate05, 5, string.Empty, "of Resurgence"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyMagickaRate06, 6, string.Empty, "of Recovery"),
            },
            "MagickaRate",
            new List<BlacklistedKeywords> {
                new(new List<BlacklistedKeyword> { new(Skyrim.Keyword.ArmorCuirass) }),
            }
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchArmorFortifyStaminaRate03, 3, string.Empty, "of Recuperation"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyStaminaRate04, 4, string.Empty, "of Rejuvenation"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyStaminaRate05, 5, string.Empty, "of Invigoration"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyStaminaRate06, 6, string.Empty, "of Renewal"),
            },
            "StaminaRate",
            new List<BlacklistedKeywords> {
                new(new List<BlacklistedKeyword> { new(Skyrim.Keyword.ArmorBoots) }),
                new(new List<BlacklistedKeyword> { new(Skyrim.Keyword.ArmorCuirass) }),
            }
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchArmorFortifyCarry01, 1, string.Empty, "of Lifting"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyCarry02, 2, string.Empty, "of Hauling"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyCarry03, 3, string.Empty, "of Strength"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyCarry04, 4, string.Empty, "of Brawn"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyCarry05, 5, string.Empty, "of the Ox"),
                new(Skyrim.ObjectEffect.EnchArmorFortifyCarry06, 6, string.Empty, "of the Mammoth"),
            },
            "Carry",
            new List<BlacklistedKeywords> {
                new(new List<BlacklistedKeyword> { new(Skyrim.Keyword.ArmorGauntlets) }),
            }
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchArmorResistFire01, 1, string.Empty, "of Resist Fire"),
                new(Skyrim.ObjectEffect.EnchArmorResistFire02, 2, string.Empty, "of Waning Fire"),
                new(Skyrim.ObjectEffect.EnchArmorResistFire03, 3, string.Empty, "of Dwindling Flames"),
                new(Skyrim.ObjectEffect.EnchArmorResistFire04, 4, string.Empty, "of Flame Suppression"),
                new(Skyrim.ObjectEffect.EnchArmorResistFire05, 5, string.Empty, "of Fire Abatement"),
                new(Skyrim.ObjectEffect.EnchArmorResistFire06, 6, string.Empty, "of the Firewalker"),
            },
            "ResistFire"
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchArmorResistFrost01, 1, string.Empty, "of Resist Frost"),
                new(Skyrim.ObjectEffect.EnchArmorResistFrost02, 2, string.Empty, "of Waning Frost"),
                new(Skyrim.ObjectEffect.EnchArmorResistFrost03, 3, string.Empty, "of Dwindling Frost"),
                new(Skyrim.ObjectEffect.EnchArmorResistFrost04, 4, string.Empty, "of Frost Suppression"),
                new(Skyrim.ObjectEffect.EnchArmorResistFrost05, 5, string.Empty, "of Frost Abatement"),
                new(Skyrim.ObjectEffect.EnchArmorResistFrost06, 6, string.Empty, "of Warmth"),
            },
            "ResistFrost"
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchArmorResistShock01, 1, string.Empty, "of Resist Shock"),
                new(Skyrim.ObjectEffect.EnchArmorResistShock02, 2, string.Empty, "of Waning Shock"),
                new(Skyrim.ObjectEffect.EnchArmorResistShock03, 3, string.Empty, "of Dwindling Shock"),
                new(Skyrim.ObjectEffect.EnchArmorResistShock04, 4, string.Empty, "of Shock Suppression"),
                new(Skyrim.ObjectEffect.EnchArmorResistShock05, 5, string.Empty, "of Shock Abatement"),
                new(Skyrim.ObjectEffect.EnchArmorResistShock06, 6, string.Empty, "of Grounding"),
            },
            "ResistShock"
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchArmorResistMagic01, 1, string.Empty, "of Resist Magic"),
                new(Skyrim.ObjectEffect.EnchArmorResistMagic02, 2, string.Empty, "of Waning Magic"),
                new(Skyrim.ObjectEffect.EnchArmorResistMagic03, 3, string.Empty, "of Dwindling Magic"),
                new(Skyrim.ObjectEffect.EnchArmorResistMagic04, 4, string.Empty, "of Magic Suppression"),
                new(Skyrim.ObjectEffect.EnchArmorResistMagic05, 5, string.Empty, "of Magic Abatement"),
                new(Skyrim.ObjectEffect.EnchArmorResistMagic06, 6, string.Empty, "of Nullification"),
            },
            "ResistMagic"
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchRobesCollegeAlteration01, 1, "Novice", "of Alteration"),
                new(Skyrim.ObjectEffect.EnchRobesCollegeAlteration02, 2, "Apprentice", "of Alteration"),
                new(Skyrim.ObjectEffect.EnchRobesCollegeAlteration03, 3, "Adept", "of Alteration"),
                new(Skyrim.ObjectEffect.EnchRobesCollegeAlteration04, 4, "Expert", "of Alteration"),
                new(Skyrim.ObjectEffect.EnchRobesCollegeAlteration05, 5, "Master", "of Alteration"),
            },
            "MageAlteration",
            new List<BlacklistedKeywords> {
                new(new List<BlacklistedKeyword> { new(Skyrim.Keyword.ArmorCuirass) }),
            }
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchRobesCollegeConjuration01, 1, "Novice", "of Conjuration"),
                new(Skyrim.ObjectEffect.EnchRobesCollegeConjuration02, 2, "Apprentice", "of Conjuration"),
                new(Skyrim.ObjectEffect.EnchRobesCollegeConjuration03, 3, "Adept", "of Conjuration"),
                new(Skyrim.ObjectEffect.EnchRobesCollegeConjuration04, 4, "Expert", "of Conjuration"),
                new(Skyrim.ObjectEffect.EnchRobesCollegeConjuration05, 5, "Master", "of Conjuration"),
            },
            "MageConjuration",
            new List<BlacklistedKeywords> {
                new(new List<BlacklistedKeyword> { new(Skyrim.Keyword.ArmorCuirass) }),
            }
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchRobesCollegeDestruction01, 1, "Novice", "of Destruction"),
                new(Skyrim.ObjectEffect.EnchRobesCollegeDestruction02, 2, "Apprentice", "of Destruction"),
                new(Skyrim.ObjectEffect.EnchRobesCollegeDestruction03, 3, "Adept", "of Destruction"),
                new(Skyrim.ObjectEffect.EnchRobesCollegeDestruction04, 4, "Expert", "of Destruction"),
                new(Skyrim.ObjectEffect.EnchRobesCollegeDestruction05, 5, "Master", "of Destruction"),
            },
            "MageDestruction",
            new List<BlacklistedKeywords> {
                new(new List<BlacklistedKeyword> { new(Skyrim.Keyword.ArmorCuirass) }),
            }
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchRobesCollegeIllusion01, 1, "Novice", "of Illusion"),
                new(Skyrim.ObjectEffect.EnchRobesCollegeIllusion02, 2, "Apprentice", "of Illusion"),
                new(Skyrim.ObjectEffect.EnchRobesCollegeIllusion03, 3, "Adept", "of Illusion"),
                new(Skyrim.ObjectEffect.EnchRobesCollegeIllusion04, 4, "Expert", "of Illusion"),
                new(Skyrim.ObjectEffect.EnchRobesCollegeIllusion05, 5, "Master", "of Illusion"),
            },
            "MageIllusion",
            new List<BlacklistedKeywords> {
                new(new List<BlacklistedKeyword> { new(Skyrim.Keyword.ArmorCuirass) }),
            }
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchRobesCollegeRestoration01, 1, "Novice", "of Restoration"),
                new(Skyrim.ObjectEffect.EnchRobesCollegeRestoration02, 2, "Apprentice", "of Restoration"),
                new(Skyrim.ObjectEffect.EnchRobesCollegeRestoration03, 3, "Adept", "of Restoration"),
                new(Skyrim.ObjectEffect.EnchRobesCollegeRestoration04, 4, "Expert", "of Restoration"),
                new(Skyrim.ObjectEffect.EnchRobesCollegeRestoration05, 5, "Master", "of Restoration"),
            },
            "MageRestoration",
            new List<BlacklistedKeywords> {
                new(new List<BlacklistedKeyword> { new(Skyrim.Keyword.ArmorCuirass) }),
            }
        ),
        new EnchantmentType(
            new List<EnchantmentVariant> {
                new(Skyrim.ObjectEffect.EnchRobesCollegeMagickaRate01, 1, "Novice"),
                new(Skyrim.ObjectEffect.EnchRobesCollegeMagickaRate02, 2, "Apprentice"),
                new(Skyrim.ObjectEffect.EnchRobesCollegeMagickaRate03, 3, "Adept"),
                new(Skyrim.ObjectEffect.EnchRobesCollegeMagickaRate04, 4, "Expert"),
                new(Skyrim.ObjectEffect.EnchRobesCollegeMagickaRate05, 5, "Master"),
            },
            "MageRegen",
            new List<BlacklistedKeywords> {
                new(new List<BlacklistedKeyword> { new(Skyrim.Keyword.ArmorCuirass) }),
                new(new List<BlacklistedKeyword> { new(Skyrim.Keyword.ArmorHelmet) }),
                new(new List<BlacklistedKeyword> { new(Skyrim.Keyword.ArmorJewelry) }),
            }
        ),
    };
}

public class EnchantmentType {
    [Tooltip("Preferably unique identifier that will be part of the editor id of an enchanted object\nExample: StaminaDamage")]
    public string EditorID  = string.Empty;
    
    [Tooltip("Leveled variants of an enchantment")]
    public List<EnchantmentVariant> Variants  = new();
    
    [Tooltip("Optional list to blacklist keywords on enchantables when one of these entries matches")]
    public List<BlacklistedKeywords> Blacklists = new();

    public EnchantmentType() {}
    public EnchantmentType(List<EnchantmentVariant> variants, string editorID, List<BlacklistedKeywords>? blacklists = null) {
        Variants = variants;
        EditorID = editorID;
        Blacklists = blacklists ?? new List<BlacklistedKeywords>();
    }
    
    public void Deconstruct(out List<EnchantmentVariant> variants, out string editorID, out List<BlacklistedKeywords> blacklists) {
        variants = Variants;
        editorID = EditorID;
        blacklists = Blacklists;
    }
}
public class EnchantmentVariant {
    [Tooltip("Effect this variant is associated to")]
    [JsonConverter(typeof(FormKeyJsonConverter))]
    public IFormLinkGetter<IObjectEffectGetter> Effect = FormLink<IObjectEffectGetter>.Null;

    [Tooltip("Tier this variant is associated to - should be unique for this enchantment type\nUsually a number between 1 and 6")]
    public int Tier;
    
    [Tooltip("Prefix to be added at the beginning of the enchanted object's name\nExample: Blessed Iron Sword")]
    public string Prefix = string.Empty;
    
    [Tooltip("Suffix to be added at the end of the enchanted object's name\nExample: Iron Sword of Weariness")]
    public string Suffix = string.Empty;

    public EnchantmentVariant() {}
    public EnchantmentVariant(IFormLinkGetter<IObjectEffectGetter> effect, int tier, string prefix = "", string suffix = "") {
        Effect = effect;
        Tier = tier;
        Prefix = prefix;
        Suffix = suffix;
    }

    public void Deconstruct(out IFormLinkGetter<IObjectEffectGetter> effect, out int tier, out string prefix, out string suffix) {
        effect = Effect;
        tier = Tier;
        prefix = Prefix;
        suffix = Suffix;
    }
    
    public virtual bool Equals(EnchantmentVariant? other) {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;

        return Effect.Equals(other.Effect);
    }
    
    public override int GetHashCode() {
        return Effect.GetHashCode();
    }
}

public class BlacklistedKeywords {
    [Tooltip("When all keywords in this list are in an enchantable, it will not be able to get this object effect")]
    public List<BlacklistedKeyword> Blacklist = new();
    
    public BlacklistedKeywords() {}
    public BlacklistedKeywords(List<BlacklistedKeyword> blacklist) {
        Blacklist = blacklist;
    }
}
public class BlacklistedKeyword {
    [Tooltip("When all keywords in this list are in an enchantable, it will not be able to get this object effect")]
    [JsonConverter(typeof(FormKeyJsonConverter))]
    public IFormLinkGetter<IKeywordGetter> BlacklistedItem  = FormLink<IKeywordGetter>.Null;


    public BlacklistedKeyword() {}
    public BlacklistedKeyword(IFormLinkGetter<IKeywordGetter> blacklistedItem) {
        BlacklistedItem = blacklistedItem;
    }
}

public static class EnchantmentTypeExtension {
    public static void Update(this List<EnchantmentType> enchantmentTypes, EnchantmentType newType) {
        var existingType = enchantmentTypes.FirstOrDefault(t => t.EditorID == newType.EditorID);
        if (existingType != null) {
            existingType.Variants.RemoveWhere(v => newType.Variants.Any(newV => v.Tier == newV.Tier));
            existingType.Variants.AddRange(newType.Variants);
        } else {
            enchantmentTypes.Add(newType);
        }
    }
    
    public static void Update(this List<EnchantmentType> enchantmentTypes, List<EnchantmentType> newTypes) {
        foreach (var newType in newTypes) enchantmentTypes.Update(newType);
    }
}