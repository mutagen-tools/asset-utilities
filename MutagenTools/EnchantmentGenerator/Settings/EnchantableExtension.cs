﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mutagen.Bethesda;
using Mutagen.Bethesda.Plugins.Cache;
using Mutagen.Bethesda.Skyrim;
using MutagenLibrary.ReferenceCache;
using Noggog;
namespace MutagenTools.EnchantmentGenerator.Settings; 

public static class EnchantableExtension {
    /// <summary>
    /// Searches for all enchantables that use this enchantable as a template
    /// </summary>
    /// <param name="enchantable">enchantable template</param>
    /// <param name="referenceQuery">reference query to check for existing variants</param>
    /// <param name="linkCache">link cache to resolve references</param>
    /// <returns>list of armors using enchantable as template</returns>
    public static List<IEnchantableGetter> GetEnchantmentVariants(this IEnchantableGetter enchantable, ReferenceQuery referenceQuery, ILinkCache linkCache) {
        var list = new List<IEnchantableGetter>();

        referenceQuery.LoadModReferences(linkCache);
        
        foreach (var reference in referenceQuery.GetReferences(enchantable.FormKey)) {
            if (reference.Type == typeof(IArmorGetter)) {
                var armor = linkCache.Resolve<IArmorGetter>(reference.FormKey);
                if (!armor.ObjectEffect.IsNull && armor.TemplateArmor.FormKey == enchantable.FormKey) {
                    list.Add(armor);
                }
            }
            
            if (reference.Type == typeof(IWeaponGetter)) {
                var weapon = linkCache.Resolve<IWeaponGetter>(reference.FormKey);
                if (!weapon.ObjectEffect.IsNull && weapon.Template.FormKey == enchantable.FormKey) {
                    list.Add(weapon);
                }
            }
        }

        return list;
    }
    
    /// <summary>
    /// Make all missing enchantment variants of an enchantable within a list of enchantments
    /// </summary>
    /// <param name="enchantable">enchantable to create variants for</param>
    /// <param name="referenceQuery">reference query to check for existing variants</param>
    /// <param name="linkCache">link cache to resolve references</param>
    /// <param name="mod">mod to add variant to</param>
    /// <param name="enchantments">enchantments to add if missing</param>
    /// <param name="prefix">prefix of the mod to make sure it stays at the editor id start</param>
    /// <returns>new enchantment variant</returns>
    /// <exception cref="ArgumentException">throws when enchantable is not of correct type armor or weapon</exception>
    public static List<IEnchantable> MakeEnchantmentVariants(this IEnchantableGetter enchantable, ReferenceQuery referenceQuery, ILinkCache linkCache, ISkyrimMod mod, List<EnchantmentType> enchantments, string prefix) {
        var existingEffect = enchantable.GetEnchantmentVariants(referenceQuery, linkCache).Select(w => w.ObjectEffect).ToList();

        referenceQuery.LoadModReferences(linkCache);

        var newEnchantables = new List<IEnchantable>();

        foreach (var (variants, editorID, _) in enchantments) {
            foreach (var enchantmentVariant in variants) {
                if (existingEffect.Contains(enchantmentVariant.Effect.FormKey)) continue;
                
                var enchanted = MakeEnchantmentVariant(enchantable, mod, editorID, enchantmentVariant, prefix);
                switch (enchantable) {
                    case Armor armor:
                        mod.Armors.Add(armor);
                        break;
                    case Weapon weapon:
                        mod.Weapons.Add(weapon);
                        break;
                }
                
                newEnchantables.Add(enchanted);
                existingEffect.Add(enchanted.ObjectEffect);
            }
        }

        return newEnchantables;
    }

    /// <summary>
    /// Makes an enchantment variant of an enchantable
    /// </summary>
    /// <param name="enchantable">enchantable to create variant for</param>
    /// <param name="mod">mod to add variant to</param>
    /// <param name="editorID">string added to the editor id by the the enchantment variant</param>
    /// <param name="enchantmentVariant">enchantment variant to add</param>
    /// <param name="prefix">prefix of the mod to make sure it stays at the editor id start</param>
    /// <returns>new enchantment variant</returns>
    /// <exception cref="ArgumentException">throws when enchantable is not of correct type armor or weapon</exception>
    public static IEnchantable MakeEnchantmentVariant(this IEnchantableGetter enchantable, ISkyrimMod mod, string editorID, EnchantmentVariant enchantmentVariant, string prefix = "") {
        if (enchantable is IArmorGetter armor) {
            var duplicateArmor = armor.Duplicate(mod.GetNextFormKey());
            duplicateArmor.TemplateArmor = armor.ToNullableLink();
            duplicateArmor.ObjectEffect = enchantmentVariant.Effect.AsNullable();
            duplicateArmor.Name = $"{enchantmentVariant.Prefix} {duplicateArmor.Name} {enchantmentVariant.Suffix}".Trim();
            duplicateArmor.EditorID = $"{prefix}Ench{enchantable.EditorID?.TrimStart(prefix)}{editorID}{enchantmentVariant.Tier:D2}";
            duplicateArmor.EnchantmentAmount = enchantmentVariant.Tier switch {
                1 => 500,
                2 => 1000,
                3 => 1500,
                4 => 2000,
                5 => 2500,
                6 => 3000,
                _ => 0
            };
            
            mod.Armors.Add(duplicateArmor);

            return duplicateArmor;
        } else if (enchantable is IWeaponGetter weapon) {
            var duplicateWeapon = weapon.Duplicate(mod.GetNextFormKey());
            duplicateWeapon.Template = weapon.ToNullableLink();
            duplicateWeapon.ObjectEffect = enchantmentVariant.Effect.AsNullable();
            duplicateWeapon.Name = $"{enchantmentVariant.Prefix} {duplicateWeapon.Name} {enchantmentVariant.Suffix}".Trim();
            duplicateWeapon.EditorID = $"{prefix}Ench{weapon.EditorID?.TrimStart(prefix)}{editorID}{enchantmentVariant.Tier:D2}";
            duplicateWeapon.EnchantmentAmount = enchantmentVariant.Tier switch {
                1 => 500,
                2 => 1000,
                3 => 1500,
                4 => 2000,
                5 => 2500,
                6 => 3000,
                _ => 0
            };
            
            mod.Weapons.Add(duplicateWeapon);

            return duplicateWeapon;
        } else {
            throw new ArgumentException("Enchantable is not armor or weapon", nameof(enchantable));
        }
    }
}

