﻿using System;
using System.Collections.Generic;
using Mutagen.Bethesda.FormKeys.SkyrimSE;
using Mutagen.Bethesda.Json;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Skyrim;
using Mutagen.Bethesda.WPF.Reflection.Attributes;
using MutagenLibrary.WPF.Json.Editor;
using MutagenLibrary.WPF.Json.Presets;
using Newtonsoft.Json;
namespace MutagenTools.EnchantmentGenerator.Settings; 

public class KeywordRulesJsonEditor : JsonEditor<List<KeywordRule>> {}
public class KeywordRulesConfigurator : PresetsConfigurator<List<KeywordRule>, KeywordRulesPreset, KeywordRulesJsonEditor> {
    public override string Header { get; set; } = "Keyword Rules";
}

public class KeywordRulesPreset : PresetManager<List<KeywordRule>, KeywordRulesPreset> {
    protected override string PresetsDirectoryName => "KeywordRules";

    protected override List<KeywordRule> DefaultData => new(
        new List<KeywordRule> {
            //Armor
            //Heavy Armor
            new(Skyrim.Keyword.ArmorMaterialIron, new List<Tier> { new(1, 1), new(2, 4), new(3, 6) }),
            new(Skyrim.Keyword.ArmorMaterialIronBanded, new List<Tier> { new(1, 3), new(2, 5), new(3, 7) }),
            new(Skyrim.Keyword.ArmorMaterialSteel, new List<Tier> { new(1, 7), new(2, 9), new(3, 11) }),
            new(Skyrim.Keyword.ArmorMaterialDwarven, new List<Tier> { new(2, 13), new(3, 15), new(4, 17) }),
            new(Skyrim.Keyword.ArmorMaterialSteelPlate, new List<Tier> { new(2, 19), new(3, 20), new(4, 21) }),
            new(Skyrim.Keyword.ArmorMaterialOrcish, new List<Tier> { new(3, 26), new(4, 28), new(5, 30) }),
            new(Skyrim.Keyword.ArmorMaterialEbony, new List<Tier> { new(3, 33), new(4, 35), new(5, 38) }),
            new(Skyrim.Keyword.ArmorMaterialDragonplate, new List<Tier> { new(4, 41), new(5, 44), new(6, 47) }),
            new(Skyrim.Keyword.ArmorMaterialDaedric, new List<Tier> { new(4, 49), new(5, 52), new(6, 55) }),
            
            //Light Armor
            new(Skyrim.Keyword.ArmorMaterialHide, new List<Tier> { new(1, 1), new(2, 4), new(3, 6) }),
            new(Skyrim.Keyword.ArmorMaterialStudded, new List<Tier> { new(1, 3), new(2, 5), new(3, 7) }),
            new(Skyrim.Keyword.ArmorMaterialLeather, new List<Tier> { new(1, 7), new(2, 9), new(3, 11) }),
            new(Skyrim.Keyword.ArmorMaterialElven, new List<Tier> { new(2, 13), new(3, 15), new(4, 17) }),
            new(Skyrim.Keyword.ArmorMaterialScaled, new List<Tier> { new(2, 20), new(3, 22), new(4, 25) }),
            new(Skyrim.Keyword.ArmorMaterialElvenGilded, new List<Tier> { new(3, 28), new(4, 31), new(5, 34) }),
            new(Skyrim.Keyword.ArmorMaterialGlass, new List<Tier> { new(3, 37), new(4, 40), new(5, 43) }),
            new(Skyrim.Keyword.ArmorMaterialDragonscale, new List<Tier> { new(4, 47), new(5, 50), new(6, 53) }),
            
            //Jewelry
            new(Skyrim.Keyword.ArmorJewelry, new List<Tier> { new(1, 1), new(2, 8), new(3, 16), new(4, 24), new(5, 32), new(6, 40) }),
            
            //Misc Armor
            // new(Skyrim.Keyword.ArmorMaterialImperialHeavy, new List<Tier> { new(1), new(2), new(3) }),
            // new(Skyrim.Keyword.ArmorMaterialImperialLight, new List<Tier> { new(1), new(2), new(3) }),
            // new(Skyrim.Keyword.ArmorMaterialImperialStudded, new List<Tier> { new(1), new(2), new(3) }),
            
            //Dawnguard Armor
            new(Dawnguard.Keyword.DLC1ArmorMaterialVampire, new List<Tier> { new(1, 1), new(2, 8), new(3, 16), new(4, 24), new(5, 32), new(6, 40) }),
            
            //Dragonborn Armor
            new(Dragonborn.Keyword.DLC2ArmorMaterialBonemoldHeavy, new List<Tier> { new(1), new(2), new(3) }),
            new(Dragonborn.Keyword.DLC2ArmorMaterialChitinHeavy, new List<Tier> { new(2, 20), new(3, 21), new(4, 25) }),
            new(Dragonborn.Keyword.DLC2ArmorMaterialNordicHeavy, new List<Tier> { new(3, 25), new(4, 27), new(5, 29) }),
            new(Dragonborn.Keyword.DLC2ArmorMaterialStalhrimHeavy, new List<Tier> { new(4, 40), new(5, 43), new(6, 46) }),
            
            new(Dragonborn.Keyword.DLC2ArmorMaterialChitinLight, new List<Tier> { new(2, 13), new(3, 15), new(4, 17) }),
            new(Dragonborn.Keyword.DLC2ArmorMaterialStalhrimLight, new List<Tier> { new(3, 36), new(4, 39), new(5, 42) }),

            //Weapons
            new(Skyrim.Keyword.WeapMaterialIron, new List<Tier> { new(1, 1), new(2, 4), new(3, 6) }),
            new(Skyrim.Keyword.WeapMaterialSteel, new List<Tier> { new(1, 4), new(2, 6), new(3, 8) }),
            new(Skyrim.Keyword.WeapMaterialOrcish, new List<Tier> { new(2, 7), new(3, 9), new(4, 11) }),
            new(Skyrim.Keyword.WeapMaterialDwarven, new List<Tier> { new(2, 13), new(3, 15), new(4, 17) }),
            new(Skyrim.Keyword.WeapMaterialElven, new List<Tier> { new(3, 20), new(4, 22), new(5, 25) }),
            new(Skyrim.Keyword.WeapMaterialGlass, new List<Tier> { new(3, 28), new(4, 31), new(5, 34) }),
            new(Skyrim.Keyword.WeapMaterialEbony, new List<Tier> { new(4, 37), new(5, 40), new(6, 43) }),
            new(Skyrim.Keyword.WeapMaterialDaedric, new List<Tier> { new(4, 47), new(5, 50), new(6, 53) }),
            
            //Misc Weapons
            new(Skyrim.Keyword.WeapMaterialDraugr, new List<Tier> { new(1), new(2), new(3) }),
            new(Skyrim.Keyword.WeapMaterialDraugrHoned, new List<Tier> { new(1), new(2), new(3) }),
            new(Skyrim.Keyword.WeapMaterialImperial, new List<Tier> { new(1), new(2), new(3) }),
            
            //Dawnguard Weapons
            new(Dawnguard.Keyword.WeapDwarvenCrossbow, new List<Tier> { new(3, 20), new(4, 22), new(5, 25) }),
            
            //Dragonborn Weapons
            new(Dragonborn.Keyword.DLC2WeaponMaterialNordic, new List<Tier> { new(3), new(4), new(5) }),
            new(Dragonborn.Keyword.DLC2WeaponMaterialStalhrim, new List<Tier> { new(4), new(5), new(6) }),
        }
    );
}

public class KeywordRule {
    public KeywordRule() {}
    public KeywordRule(IFormLinkGetter<IKeywordGetter> keyword, List<Tier> tiers) {
        Keyword = keyword;
        Tiers = tiers;
    }
    
    [Tooltip("Weapons/Armor with this keyword will be included in this rule")]
    [JsonConverter(typeof(FormKeyJsonConverter))]
    public IFormLinkGetter<IKeywordGetter> Keyword = FormLink<IKeywordGetter>.Null;
    
    [Tooltip("Enchantments of Tiers in this interval will be included")]
    public List<Tier> Tiers = new();
}

public class Tier {
    public Tier() {}
    public Tier(int tierIndex, short level = 1) {
        TierIndex = tierIndex;
        Level = level;
    }

    [Tooltip("Tier index that corresponds to enchantment tiers")]
    public int TierIndex = 1;
    
    [Tooltip("Level of items in a leveled list")]
    public short Level = 1;

    public override bool Equals(object? obj) {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        return obj.GetType() == GetType() && Equals((Tier) obj);
    }
    
    public bool Equals(Tier tier) {
        return TierIndex == tier.TierIndex;
    }
    
    public override int GetHashCode() {
        return HashCode.Combine(TierIndex);
    }
}
