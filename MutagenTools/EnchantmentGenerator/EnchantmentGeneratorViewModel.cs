﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Mutagen.Bethesda.FormKeys.SkyrimSE;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Plugins.Aspects;
using Mutagen.Bethesda.Skyrim;
using MutagenLibrary.Extension;
using MutagenLibrary.WPF.UtilityTypes;
using MutagenTools.Core;
using MutagenTools.EnchantmentGenerator.EnchantmentComposite;
using MutagenTools.EnchantmentGenerator.Settings;
using Noggog;
using Noggog.WPF;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
namespace MutagenTools.EnchantmentGenerator;

public class EnchantmentGeneratorViewModel : ViewModel {
    private readonly SkyrimMod _patchMod = new(new ModKey("EnchantedGear", ModType.Plugin), SkyrimRelease.SkyrimSE);
    
    [Reactive] public bool IsLoading { get; set; }
    [Reactive] public string LoadText { get; set; } = string.Empty;

    [Reactive] public ModItem? SelectedMod { get; set; }
    
    [Reactive] public string Prefix { get; set; } = string.Empty;
    
    [Reactive] public ObservableCollection<AbstractEnchantable> EnchantmentRecords { get; set; } = new();
    [Reactive] public ObservableCollection<object> SelectedEnchantmentRecords { get; set; } = new();
    
    public ICommand MakeEnchantment { get; }
    public ICommand AddLeveledList { get; }

    public bool CanMakeEnchantment() => SelectedEnchantmentRecords.Cast<AbstractEnchantable>().Any(record => record.HasEnchantment is null or false);
    public bool CanAddLeveledList() => SelectedEnchantmentRecords.Cast<AbstractEnchantable>().Any(record => record.HasEnchantment is not false && record.InLeveledList is null or false);

    private readonly Dictionary<FormKey, List<IEnchantable>> _addedVariants = new();

    public EnchantmentGeneratorViewModel() {
        this.WhenAnyValue(x => x.SelectedMod, x => x.Prefix)
            .Throttle(TimeSpan.FromMilliseconds(250), RxApp.MainThreadScheduler)
            .Subscribe(_ => UpdateRecordsAsync());

        AddLeveledList = ReactiveCommand.CreateFromTask(AddLeveledListAsync);
        MakeEnchantment = ReactiveCommand.CreateFromTask(MakeEnchantmentAsync);
        
        EnchantmentsPreset.Instance.Value.PresetChanged += async (_, _) => await UpdateRecordsAsync().ConfigureAwait(false);
        KeywordRulesPreset.Instance.Value.PresetChanged += async (_, _) => await UpdateRecordsAsync().ConfigureAwait(false);
    }

    public async Task MakeEnchantmentAsync() {
        IsLoading = true;

        await Task.Run(() => {
            foreach (var record in SelectedEnchantmentRecords.Cast<AbstractEnchantable>()) {
                if (record.HasEnchantment is true) continue;

                var enchantable = record.TryGetEnchantable();
                if (enchantable == null) continue;
                
                LoadText = $"Processing {enchantable.EditorID}";

                //Make missing enchantment variants for each selected entry
                var newVariants = enchantable.MakeEnchantmentVariants(
                    SharedVariables.ReferenceQuery,
                    SharedVariables.LinkCache,
                    _patchMod,
                    record.MissingEnchantments,
                    Prefix
                );

                //Add them to cache
                foreach (var newVariant in newVariants) {
                    _addedVariants.GetOrAdd(enchantable.FormKey, () => new List<IEnchantable>()).Add(newVariant);
                }

                switch (record) {
                    case EnchantedRecord enchantedRecord: //Subtree selected
                        enchantedRecord.AddNewRecord(newVariants[0]);
                        enchantedRecord.HasEnchantment = true;
                        enchantedRecord.Parent.UpdateHasEnchantment();
                        break;
                    case BaseRecord baseRecord: //Tree root selected
                        baseRecord.HasEnchantment = true;
                        foreach (var child in baseRecord.Children.Cast<EnchantedRecord>()) {
                            if (child.HasEnchantment is true) continue;

                            var missingEffect = child.MissingEnchantments[0].Variants[0].Effect;
                            var newVariant = newVariants.Find(v => v.ObjectEffect.FormKey == missingEffect.FormKey);

                            if (newVariant == null) continue;

                            child.AddNewRecord(newVariant);
                            child.HasEnchantment = true;
                        }
                        break;
                }
            }
    
            MoveEngine.WriteMod(_patchMod);
        }).ConfigureAwait(false);
        
        IsLoading = false;
    }

    public async Task AddLeveledListAsync() {
        short GetLevel(IKeywordedGetter? keyworded, int tier) {
            short level = 1;
            var validPreset = KeywordRulesPreset.Data.FirstOrDefault(p => keyworded?.Keywords != null && keyworded.Keywords.Contains(p.Keyword));
            if (validPreset != null) level = validPreset.Tiers.FirstOrDefault(t => t.TierIndex == tier)?.Level ?? 1;
            return level;
        }

        IsLoading = true;

        await Task.Run(() => {
            foreach (var record in SelectedEnchantmentRecords.Cast<AbstractEnchantable>()) {
                if (record.InLeveledList is true) continue;
                
                var enchantable = record.TryGetEnchantable();
                if (enchantable == null) continue;

                var keyworded = enchantable as IKeywordedGetter;

                LoadText = $"Processing {enchantable.EditorID}";

                switch (record) {
                    case EnchantedRecord enchanted:
                        if (enchanted.Enchantment == null) continue;
                        
                        var tier = enchanted.GetTier();
                        var level = GetLevel(keyworded, tier);

                        var sublist = enchantable.AddToSubLeveledList(SharedVariables.LinkCache, _patchMod, enchanted.Enchantment.EditorID, tier, level, Prefix);
                        if (sublist != null) {
                            enchantable.AddToLeveledLists(SharedVariables.LinkCache, _patchMod, sublist, enchanted.Enchantment.EditorID, Prefix);
                        }

                        enchanted.InLeveledList = true;
                        enchanted.Parent.UpdateInLeveledList();
                        break;
                    case BaseRecord baseRecord: {
                        foreach (var enchantedSubRecord in baseRecord.Children.Cast<EnchantedRecord>()) {
                            if (enchantedSubRecord.FormKey.IsNull) continue;
                            if (enchantedSubRecord.InLeveledList is true) continue;
                            if (enchantedSubRecord.Enchantment == null) continue;

                            var enchantedRecord = enchantedSubRecord.TryGetEnchantable();
                            if (enchantedRecord == null) continue;

                            var subRecordTier = enchantedSubRecord.GetTier();
                            var level2 = GetLevel(keyworded, subRecordTier);
                            
                            var sublist2 = enchantedRecord.AddToSubLeveledList(SharedVariables.LinkCache, _patchMod, enchantedSubRecord.Enchantment.EditorID, subRecordTier, level2, Prefix);
                            if (sublist2 != null) {
                                enchantedRecord.AddToLeveledLists(SharedVariables.LinkCache, _patchMod, sublist2, enchantedSubRecord.Enchantment.EditorID, Prefix);
                            }
                            
                            enchantedSubRecord.InLeveledList = true;
                        }
                        
                        baseRecord.UpdateInLeveledList();
                        break;
                    }
                }
            }

            MoveEngine.WriteMod(_patchMod);
        }).ConfigureAwait(false);
        
        IsLoading = false;
    }

    private async Task UpdateRecordsAsync() {
        var mod = SharedVariables.Environment.ResolveMod(SelectedMod?.ModKey);
        if (mod == null) return;

        IsLoading = true;

        //Prepare ref cache
        LoadText = "Building References";
        await Task.Run(() => SharedVariables.ReferenceQuery.LoadModReferences(SharedVariables.Environment)).ConfigureAwait(false);
        
        //Async get all valid spells from the selected mod
        LoadText = "Collecting Records";

        
        bool ValidEnchantable(IEnchantableGetter record) {
            //No enchantment allowed
            if (!record.ObjectEffect.IsNull) return false;
            
            //Must be obtainable and no template allowed
            if (record is IArmorGetter armor) {
                if ((armor.MajorFlags & Armor.MajorFlag.NonPlayable) != 0
                 || !armor.TemplateArmor.IsNull) return false;
            } else if (record is IWeaponGetter weapon) {
                if ((weapon.MajorFlags & Weapon.MajorFlag.NonPlayable) != 0
                 || !weapon.Template.IsNull) return false;
            }

            //Can't have magic disallow enchanting keyword
            var keyworded = record as IKeywordedGetter;
            if (keyworded?.Keywords != null) {
                if (keyworded.Keywords.Any(k => k.FormKey == Skyrim.Keyword.MagicDisallowEnchanting.FormKey)) {
                    return false;
                }
            }

            return true;
        }
        
        var records = new List<BaseRecord>();
        var armorTask = Task.Run(() => mod.EnumerateMajorRecords<IArmorGetter>().Where(ValidEnchantable)
            .ForEach(record => records.Add(new BaseRecord(record, _addedVariants.TryGetValue(record.FormKey, out var variants) ? variants : Array.Empty<IEnchantable>(), Prefix))));

        var weaponsTask = Task.Run(() => mod.EnumerateMajorRecords<IWeaponGetter>().Where(ValidEnchantable)
            .ForEach(record => records.Add(new BaseRecord(record, _addedVariants.TryGetValue(record.FormKey, out var variants) ? variants : Array.Empty<IEnchantable>(), Prefix))));
        
        await armorTask.ConfigureAwait(false);
        await weaponsTask.ConfigureAwait(false);

        records.RemoveAll(x => !x.Children.Any());
        EnchantmentRecords.Clear();
        EnchantmentRecords.AddRange(records);
        
        IsLoading = false;
    }
}
