using System.Reactive.Disposables;
using System.Windows.Controls;
using System.Windows.Input;
using MutagenLibrary.WPF.CustomElements.DataGrids;
using Noggog.WPF;
using ReactiveUI;
namespace MutagenTools.EnchantmentGenerator;

public class EnchantmentGeneratorViewBase : NoggogUserControl<EnchantmentGeneratorViewModel> { }

public partial class EnchantmentGenerator {
    public EnchantmentGenerator() {
        InitializeComponent();

        DataContext = ViewModel = new EnchantmentGeneratorViewModel();

        this.WhenActivated(disposable => {
            //Plugin
            this.Bind(ViewModel, x => x.SelectedMod, x => x.SourcePlugin.SelectedModItem)
                .DisposeWith(disposable);
        });
        
        //Commands
        TreeGrid.ContextMenu ??= new ContextMenu();
        
        var makeEnchantedVersion = new CommandBinding { Command = ItemView.GenericCommand };
        makeEnchantedVersion.Executed += async (_, _) => await ViewModel.MakeEnchantmentAsync().ConfigureAwait(false);
        makeEnchantedVersion.CanExecute += (_, args) => args.CanExecute = ViewModel.CanMakeEnchantment();
        
        TreeGrid.ContextMenu.Items.Add(new MenuItem {
            Header = "Make Enchanted Version(s)",
            Command = ItemView.GenericCommand,
            CommandBindings = { makeEnchantedVersion }
        });
        
        var addToLeveledList = new CommandBinding { Command = ItemView.GenericCommand };
        addToLeveledList.Executed += async (_, _) => await ViewModel.AddLeveledListAsync().ConfigureAwait(false);
        addToLeveledList.CanExecute += (_, args) => args.CanExecute = ViewModel.CanAddLeveledList();

        TreeGrid.ContextMenu.Items.Add(new MenuItem {
            Header = "Add to Leveled List",
            Command = ItemView.GenericCommand,
            CommandBindings = { addToLeveledList }
        });
    }
}