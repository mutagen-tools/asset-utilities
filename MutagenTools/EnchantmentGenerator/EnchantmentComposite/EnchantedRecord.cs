﻿using System.Collections.Generic;
using System.Linq;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Skyrim;
using MutagenLibrary.Extension.Record;
using MutagenTools.Core;
using MutagenTools.EnchantmentGenerator.Settings;
using MutagenTools.Logging;
namespace MutagenTools.EnchantmentGenerator.EnchantmentComposite;

public class EnchantedRecord : AbstractEnchantable {
    public BaseRecord Parent { get; set; }
    public IEnchantableGetter? Record { get; set; }
    public EnchantmentType? Enchantment { get; set; }

    public static EnchantedRecord? ParseEnchantedRecord(IEnchantableGetter enchantable, BaseRecord parent) {
        if (enchantable.EditorID == null) return null;

        //Guess tier
        var objectEffect = enchantable.ObjectEffect.TryResolve(SharedVariables.LinkCache);
        var tier = 1;
        if (int.TryParse(enchantable.EditorID[^2..], out var tier1)) tier = tier1;
        else if (int.TryParse(enchantable.EditorID[^1..], out var tier2)) tier = tier2;
        else {
            if (objectEffect is { EditorID: {} }) {
                if (int.TryParse(objectEffect.EditorID[^2..], out var tier3)) tier = tier3;
                if (int.TryParse(objectEffect.EditorID[^1..], out var tier4)) tier = tier4;
            } else {
                tier = 1;
                Log.Warning($"Tier for {enchantable.FormKey} {enchantable.EditorID} couldn't be determined - use 1 as default");
            }
        }

        //Find matching enchantment variant
        foreach (var (variants, editorID, _) in EnchantmentsPreset.Data) {
            foreach (var variant in variants) {
                if (variant.Effect.FormKey == enchantable.ObjectEffect.FormKey && tier == variant.Tier) {
                    return new EnchantedRecord(
                        enchantable,
                        parent,
                        new EnchantmentType(new List<EnchantmentVariant> { variant }, editorID)
                    );
                }
            }
        }

        return null;
    }
    
    /// <summary>
    /// Constructor for existing records
    /// </summary>
    public EnchantedRecord(IEnchantableGetter enchantable, BaseRecord parent, EnchantmentType enchantment) : base(enchantable) {
        HasEnchantment = true;
        InLeveledList = true;
        Parent = parent;
        Record = enchantable;
        Enchantment = enchantment;
        InLeveledList = enchantable.IsInEnchantingSublist(SharedVariables.LinkCache, SharedVariables.ReferenceQuery, GetTier());
    }

    /// <summary>
    /// Constructor for placeholder records
    /// </summary>
    public EnchantedRecord(IEnchantableGetter enchantable, string editorID, List<EnchantmentType> missingEnchantments, BaseRecord parent) : base(enchantable) {
        HasEnchantment = false;
        InLeveledList = false;
        FormKey = FormKey.Null;
        EditorID = editorID;
        MissingEnchantments = missingEnchantments;
        Parent = parent;
    }

    //Only call when constructed with the constructor with missing enchantments
    public void AddNewRecord(IEnchantableGetter record) {
        FormKey = record.FormKey;
        EditorID = record.EditorID ?? string.Empty;
        RecordType = record.Registration.ClassType.Name;
        Record = record;
        
        Enchantment = MissingEnchantments[0];
        MissingEnchantments.Clear();
    }

    public int GetTier() {
        if (Enchantment == null || !Enchantment.Variants.Any()) return 1;

        return Enchantment.Variants[0].Tier;
    }

    public override IEnchantableGetter? TryGetEnchantable() {
        if (Record != null) return Record;

        SharedVariables.LinkCache.TryResolve<IEnchantableGetter>(Parent.FormKey, out var enchantable);
        return enchantable;
    }
}
