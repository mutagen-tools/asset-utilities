﻿using System.Collections.Generic;
using System.Linq;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Plugins.Aspects;
using Mutagen.Bethesda.Skyrim;
using MutagenLibrary.Extension.Record;
using MutagenTools.Core;
using MutagenTools.EnchantmentGenerator.Settings;
using Noggog;
namespace MutagenTools.EnchantmentGenerator.EnchantmentComposite;

public class BaseRecord : AbstractEnchantable {
    public List<AbstractEnchantable> Children { get; set; }

    public BaseRecord(IEnchantableGetter enchantable, IEnumerable<IEnchantable> addedVariants, string prefix) : base(enchantable) {
        //Get existing variants from environment and added variants
        var existingVariants = enchantable.GetEnchantmentVariants(SharedVariables.ReferenceQuery, SharedVariables.LinkCache);
        existingVariants = existingVariants.Concat(addedVariants.Where(added => existingVariants.All(e => e.ObjectEffect.FormKey != added.ObjectEffect.FormKey))).ToList();

        Children = existingVariants
            .Select(e => EnchantedRecord.ParseEnchantedRecord(e, this))
            .NotNull()
            .Cast<AbstractEnchantable>()
            .ToList();

        var keyworded = (enchantable as IKeywordedGetter)!;
        var keywordRules = keyworded.Keywords == null ? new List<KeywordRule>() : KeywordRulesPreset.Data.Where(x => keyworded.Keywords.Contains(x.Keyword)).ToList();

        //Loop through all active enchantments and add missing enchantments as child records
        foreach (var (enchantmentVariants, editorID, blacklists) in EnchantmentsPreset.Data) {
            var missingVariants = new List<EnchantmentVariant>();
            
            //This enchantment won't be accepted when the tier doesn't provide a variant for every tier in the list
            var lowestTier = enchantmentVariants.Min(x => x.Tier);
            if (keywordRules.Count > 0 && keywordRules.All(r => lowestTier > r.Tiers.Min(t => t.TierIndex))) continue;

            foreach (var variant in enchantmentVariants) {
                //At least one given keyword rules must include the current tier
                if (keywordRules.Count > 0 && !keywordRules.All(r => r.Tiers.Any(t => t.TierIndex == variant.Tier))) continue;
                
                //No blacklist matches all keywords in the enchantable
                if (keyworded.Keywords != null && blacklists.Any() && blacklists.Any(b => b.Blacklist.All(k => keyworded.Keywords.Contains(k.BlacklistedItem)))) continue;

                var variantObjectEffect = variant.Effect.TryResolve(SharedVariables.LinkCache);
                if (variantObjectEffect == null) continue;
                
                //Select all keyword that are referenced by worn restrictions
                var wornRestrictions = variantObjectEffect.GetWornRestrictions(SharedVariables.LinkCache)?.TryResolve(SharedVariables.LinkCache);
                var keywords = wornRestrictions?.Items.Select(i => i.FormKey).ToList() ?? new List<FormKey>();

                if (((enchantable is IArmorGetter && variantObjectEffect.IsArmorEnchantment()) || (enchantable is IWeaponGetter && variantObjectEffect.IsWeaponEnchantment()))    //Valid armor/weapon enchantment
                 && (!keywords.Any() || (keyworded is { Keywords: {} } && keyworded.Keywords.Any(k => keywords.Contains(k.FormKey)))) //Valid keywords
                 && existingVariants.All(v => v.ObjectEffect.FormKey != variant.Effect.FormKey)) {                                                     //Not already existing
                    missingVariants.Add(variant);
                    Children.Add(new EnchantedRecord(
                            enchantable,
                            $"{prefix}Ench{enchantable.EditorID?.TrimStart(prefix)}{editorID}{variant.Tier:D2}",
                            new List<EnchantmentType> { new(new List<EnchantmentVariant> { variant }, editorID)},
                            this
                        )
                    );
                }
            }

            if (missingVariants.Any()) MissingEnchantments.Add(new EnchantmentType(missingVariants, editorID));
        }

        UpdateInLeveledList();

        if (existingVariants.Count == 0) {
            HasEnchantment = false;
        } else if (MissingEnchantments.Count == 0) {
            HasEnchantment = true;
        }
    }

    public void UpdateHasEnchantment() {
        if (Children.Any() && Children.All(c => c.HasEnchantment is true)) {
            HasEnchantment = true;
        } else if (Children.Any(c => c.HasEnchantment is true)) {
            HasEnchantment = null;
        } else {
            HasEnchantment = false;
        }
    }

    public void UpdateInLeveledList() {
        if (Children.Any() && Children.All(c => c.InLeveledList is true)) {
            InLeveledList = true;
        } else if (Children.Any(c => c.InLeveledList is true)) {
            InLeveledList = null;
        } else {
            InLeveledList = false;
        }
    }
}
