﻿using System.Collections.Generic;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Plugins.Records;
using Mutagen.Bethesda.Skyrim;
using MutagenTools.Core;
using MutagenTools.EnchantmentGenerator.Settings;
using Noggog;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
namespace MutagenTools.EnchantmentGenerator.EnchantmentComposite;

public class RecordIdentifierItem : ReactiveObject, ISelectable {
    [Reactive]
    public bool IsSelected { get; set; }

    [Reactive]
    public FormKey FormKey { get; set; }
    
    [Reactive]
    public string EditorID { get; set; }

    [Reactive]
    public string RecordType { get; set; }

    public RecordIdentifierItem(IMajorRecordGetter record) => SetRecord(record);

    public virtual void SetRecord(IMajorRecordGetter record) {
        FormKey = record.FormKey;
        EditorID = record.EditorID ?? string.Empty;
        RecordType = record.Registration.ClassType.Name;
    }
}

public abstract class AbstractEnchantable : RecordIdentifierItem {
    [Reactive]
    public bool? HasEnchantment { get; set; }
    
    [Reactive]
    public bool? InLeveledList { get; set; }
    
    public List<EnchantmentType> MissingEnchantments { get; set; } = new();

    protected AbstractEnchantable(IEnchantableGetter enchantable) : base(enchantable) {}
    
    public virtual IEnchantableGetter? TryGetEnchantable() {
        SharedVariables.LinkCache.TryResolve<IEnchantableGetter>(FormKey, out var enchantable);
        return enchantable;
    }
}
