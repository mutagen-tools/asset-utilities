﻿using System;
using System.Linq;
using Mutagen.Bethesda;
using Mutagen.Bethesda.FormKeys.SkyrimSE;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Plugins.Cache;
using Mutagen.Bethesda.Plugins.Records;
using Mutagen.Bethesda.Skyrim;
using Noggog;
namespace MutagenTools.EnchantmentGenerator;

public static class LeveledItemExtent {
    private static readonly StringComparer StringComparer = StringComparer.OrdinalIgnoreCase;
    
    public static bool IsArmorSublist(this ILeveledItemGetter leveledItem, IArmorGetter baseArmor, int tier, string prefix = "") {
        //Example: EnchArmorElvenGauntletsOneHanded02, check for SublistEnchArmorElvenGauntlets02
        // base armor EditorID: ArmorElvenGauntlets needs to be included in SublistEnchArmorElvenGauntlets02
        // 02 of EnchArmorElvenGauntletsOneHanded02 needs to be included in SublistEnchArmorElvenGauntlets02
        return baseArmor.EditorID != null && IsArmorSublist(leveledItem.EditorID, baseArmor.EditorID.TrimStart(prefix), tier);
    }

    private static bool IsArmorSublist(string? leveledItemEditorId, string? armorEditorId, int tier) {
        return leveledItemEditorId != null && armorEditorId != null && leveledItemEditorId.Contains(armorEditorId, StringComparison.OrdinalIgnoreCase) && leveledItemEditorId.Contains(tier.ToString());
    }
    
    public static bool IsWeaponSublist(this ILeveledItemGetter leveledItem, IWeaponGetter weapon, string prefix = "") {
        //Example: EnchElvenSwordSoulTrap04, check for SublistEnchElvenSwordSoulTrap
        // EnchElvenSwordSoulTrap04 without tier needs to be included in SublistEnchElvenSwordSoulTrap
        return weapon.EditorID != null && IsWeaponSublist(leveledItem.EditorID, weapon.EditorID.TrimStart(prefix));
    }

    private static bool IsWeaponSublist(string? leveledItemEditorId, string? weaponEditorId) {
        return leveledItemEditorId != null && weaponEditorId != null && leveledItemEditorId.Contains(weaponEditorId[..^2], StringComparison.OrdinalIgnoreCase);
    }
    
    /// <summary>
    /// Adds an enchantable object to it's leveled list, and creates the leveled list,
    /// if it doesn't yet exist in the environment. The procedure to find the correct
    /// leveled list is fully determined by convention, so results may introduce more
    /// new leveled lists than necessary.
    /// 
    /// For specifics how leveled lists armor found, see IsArmorSublist and IsWeaponSublist.
    ///  
    /// </summary>
    /// <param name="enchantable">enchantable to be added to a leveled list</param>
    /// <param name="linkCache">link cache to resolve references</param>
    /// <param name="mod">Mod to add overrides to</param>
    /// <param name="enchantingEditorId">EditorID part of the enchanting variant</param>
    /// <param name="tier">Tier of the enchantable passed in</param>
    /// <param name="level">Level of  the enchantable in the leveled list</param>
    /// <param name="prefix">prefix of the mod to make sure it stays at the editor id start and sub-lists are properly detected by editor id</param>
    public static ILeveledItem? AddToSubLeveledList<TMod, TModGetter>(this IEnchantableGetter enchantable, ILinkCache<TMod, TModGetter> linkCache, TMod mod, string enchantingEditorId, int tier, short level, string prefix)
        where TModGetter : class, IModGetter
        where TMod : class, TModGetter, IMod {
        switch (enchantable) {
            case IArmorGetter armor:
                return AddArmorOrJewelryToSubLeveledList(armor, linkCache, mod, enchantingEditorId, tier, level, prefix);
            case IWeaponGetter weapon:
                return AddWeaponToSubLeveledList(weapon, linkCache, mod, enchantingEditorId, level, prefix);
        }

        return null;
    }
    
    private static ILeveledItem? AddArmorOrJewelryToSubLeveledList<TMod, TModGetter>(IArmorGetter armor, ILinkCache<TMod, TModGetter> linkCache, TMod mod, string enchantingEditorId, int tier, short level, string prefix = "")
        where TModGetter : class, IModGetter
        where TMod : class, TModGetter, IMod {
        if (armor.EditorID == null) return null;
        
        if (armor.Keywords != null && armor.Keywords.Any(k => k.FormKey == Skyrim.Keyword.ArmorJewelry.FormKey)) {
            return AddJewelryToSubLeveledList(armor, linkCache, mod, enchantingEditorId, level, prefix);
        }
        
        return AddArmorToSubLeveledList(armor, linkCache, mod, tier, prefix);
    }
    
    private static ILeveledItem? AddArmorToSubLeveledList<TMod, TModGetter>(IArmorGetter armor, ILinkCache<TMod, TModGetter> linkCache, TMod mod, int tier, string prefix)
        where TModGetter : class, IModGetter
        where TMod : class, TModGetter, IMod {

        var baseArmor = armor.TemplateArmor.TryResolve(linkCache);
        if (baseArmor?.EditorID == null) return null;

        var basePrefixFreeEditorID = baseArmor.EditorID.TrimStart(prefix);
        var leveledItemGroup = mod.GetTopLevelGroup<LeveledItem>();

        //Find armor sublist that fits
        ILeveledItem? sublist = leveledItemGroup.Records.FirstOrDefault(leveledItem => leveledItem.IsArmorSublist(baseArmor, tier, prefix));
        if (sublist == null) {
            foreach (var leveledItem in linkCache.AllIdentifiers<ILeveledItemGetter>()) {
                if (!IsArmorSublist(leveledItem.EditorID, basePrefixFreeEditorID, tier)) continue;

                var context = linkCache.ResolveContext<ILeveledItem, ILeveledItemGetter>(leveledItem.FormKey);

                //Don't use any leveled lists that include armor with other template weapons
                if (context.Record.Entries != null && !context.Record.Entries.All(x => {
                    var item = x.Data?.Reference.TryResolve(linkCache);
                    if (item == null) return true;

                    return item switch {
                        IArmorGetter armorEntry => armorEntry.TemplateArmor.Equals(armor.TemplateArmor),
                        _ => false
                    };
                })) {
                    continue;
                }

                sublist = context.GetOrAddAsOverride(mod);

                break;
            }
        }

        //Otherwise create new armor sublist
        sublist ??= new LeveledItem(mod.GetNextFormKey(), SkyrimRelease.SkyrimSE) {
            ChanceNone = 0,
            EditorID = $"{prefix}SublistEnch{basePrefixFreeEditorID}{tier:D2}",
            Flags = LeveledItem.Flag.CalculateForEachItemInCount | LeveledItem.Flag.CalculateFromAllLevelsLessThanOrEqualPlayer
        };

        sublist.Entries ??= new ExtendedList<LeveledItemEntry>();
        if (sublist.Entries.Select(e => e.Data)
            .NotNull()
            .All(d => d.Reference.FormKey != armor.FormKey)) {
            sublist.Entries.Add(new LeveledItemEntry {
                Data = new LeveledItemEntryData {
                    Count = 1,
                    Level = 1,
                    Reference = new FormLink<IItemGetter>(armor)
                }
            });
        }

        AddLeveledList<TMod, TModGetter>(mod, sublist);

        return sublist;
    }

    public static ILeveledItem? AddJewelryToSubLeveledList<TMod, TModGetter>(IArmorGetter jewelry, ILinkCache<TMod, TModGetter> linkCache, TMod mod, string enchantingEditorId, short level, string prefix = "")
        where TModGetter : class, IModGetter
        where TMod : class, TModGetter, IMod {
        if (jewelry.EditorID == null || jewelry.Keywords == null) return null;

        var objectEffect = jewelry.ObjectEffect.TryResolve(linkCache);
        if (objectEffect == null) return null;

        string? jewelryStr = null;
        foreach (var keyword in jewelry.Keywords) {
            if (keyword.FormKey == Skyrim.Keyword.ClothingRing.FormKey) {
                jewelryStr = "Ring";
                break;
            }
            if (keyword.FormKey == Skyrim.Keyword.ClothingNecklace.FormKey) {
                jewelryStr = "Necklace";
                break;
            }
            if (keyword.FormKey == Skyrim.Keyword.ClothingCirclet.FormKey) {
                jewelryStr = "Circlet";
                break;
            }
        }

        if (jewelryStr == null) return null;

        var jewelrySublistEditorID = $"{prefix}LItemEnch{jewelryStr}{enchantingEditorId}";
        
        var leveledItemGroup = mod.GetTopLevelGroup<LeveledItem>();
        var sublist = leveledItemGroup.Records
                .FirstOrDefault(leveledItem => StringComparer.Equals(leveledItem.EditorID, jewelrySublistEditorID))
            ?? linkCache.AllIdentifiers<ILeveledItemGetter>()
                .Where(l => StringComparer.Equals(l.EditorID, jewelrySublistEditorID))
                .Select(l => linkCache.ResolveContext<ILeveledItem, ILeveledItemGetter>(l.FormKey))
                .Select(c => c.GetOrAddAsOverride(mod))
                .FirstOrDefault();

        //Otherwise create new jewelry sublist
        sublist ??= new LeveledItem(mod.GetNextFormKey(), SkyrimRelease.SkyrimSE) {
            ChanceNone = 0,
            EditorID = jewelrySublistEditorID,
            Flags = LeveledItem.Flag.CalculateForEachItemInCount | LeveledItem.Flag.CalculateFromAllLevelsLessThanOrEqualPlayer
        };

        sublist.Entries ??= new ExtendedList<LeveledItemEntry>();
        if (sublist.Entries.Select(e => e.Data)
            .NotNull()
            .All(d => d.Reference.FormKey != jewelry.FormKey)) {
            sublist.Entries.Add(new LeveledItemEntry {
                Data = new LeveledItemEntryData {
                    Count = 1,
                    Level = level,
                    Reference = new FormLink<IItemGetter>(jewelry)
                }
            });
        }

        AddLeveledList<TMod, TModGetter>(mod, sublist);

        return sublist;
    }

    public static ILeveledItem? AddWeaponToSubLeveledList<TMod, TModGetter>(IWeaponGetter weapon, ILinkCache<TMod, TModGetter> linkCache, TMod mod, string enchantingEditorId, short level, string prefix = "")
        where TModGetter : class, IModGetter
        where TMod : class, TModGetter, IMod {
        if (weapon.EditorID == null) return null;

        var prefixFreeEditorID = weapon.EditorID.TrimStart(prefix);
        var leveledItemGroup = mod.GetTopLevelGroup<LeveledItem>();

        //Find weapon sublist that fits
        ILeveledItem? sublist = leveledItemGroup.Records.FirstOrDefault(leveledItem => leveledItem.IsWeaponSublist(weapon, prefix));
        if (sublist == null) {
            foreach (var leveledItem in linkCache.AllIdentifiers<ILeveledItemGetter>()) {
                if (!IsWeaponSublist(leveledItem.EditorID, prefixFreeEditorID)) continue;

                var context = linkCache.ResolveContext<ILeveledItem, ILeveledItemGetter>(leveledItem.FormKey);

                //Don't use any leveled lists that include weapons with other template weapons
                if (context.Record.Entries != null && !context.Record.Entries.All(x => {
                    var item = x.Data?.Reference.TryResolve(linkCache);
                    if (item == null) return true;

                    return item switch {
                        IWeaponGetter weaponEntry => weaponEntry.Template.Equals(weapon.Template),
                        _ => false
                    };
                })) {
                    continue;
                }

                sublist = context.GetOrAddAsOverride(mod);
                break;
            }
        }

        //Otherwise create new weapon sublist
        sublist ??= new LeveledItem(mod.GetNextFormKey(), SkyrimRelease.SkyrimSE) {
            ChanceNone = 0,
            EditorID = $"{prefix}Sublist{prefixFreeEditorID.TrimEnd(Digits)}"
        };

        sublist.Entries ??= new ExtendedList<LeveledItemEntry>();
        if (sublist.Entries.Select(e => e.Data)
            .NotNull()
            .All(d => d.Reference.FormKey != weapon.FormKey)) {
            sublist.Entries.Add(new LeveledItemEntry {
                Data = new LeveledItemEntryData {
                    Count = 1,
                    Level = level,
                    Reference = new FormLink<IItemGetter>(weapon)
                }
            });
        }

        AddLeveledList<TMod, TModGetter>(mod, sublist);

        return sublist;
    }
    
    private static void AddLeveledList<TMod, TModGetter>(TMod mod, ILeveledItem list)
        where TModGetter : class, IModGetter
        where TMod : class, TModGetter, IMod {
        var leveledItemGroup = mod.GetTopLevelGroup<LeveledItem>();
        if (!leveledItemGroup.ContainsKey(list.FormKey)) {
            leveledItemGroup.Add((list as LeveledItem)!);
        }
    }
    
    public static void AddToLeveledLists<TMod, TModGetter>(this IEnchantableGetter enchantable, ILinkCache<TMod, TModGetter> linkCache, TMod mod, ILeveledItem sublist, string enchantingEditorId, string prefix)
        where TModGetter : class, IModGetter
        where TMod : class, TModGetter, IMod {
        switch (enchantable) {
            case IWeaponGetter weapon:
                if (weapon.EditorID == null || sublist.EditorID == null) return;
                
                //Attempt to add sublist to further leveled lists
                //  WeaponEnchantmentList: ListEnchElvenSword
                //  WeaponTypeList:        ListEnchElvenWeaponSoulTrap

                var leveledItemGroup = mod.GetTopLevelGroup<LeveledItem>();

                var weaponEnchantmentListEditorID = $"{prefix}ListEnch{sublist.EditorID.TrimWeaponEnchantmentList(prefix, enchantingEditorId)}";
                var weaponEnchantmentList = 
                    leveledItemGroup.Records
                        .FirstOrDefault(leveledItem => StringComparer.Equals(leveledItem.EditorID, weaponEnchantmentListEditorID))
                 ?? linkCache.AllIdentifiers<ILeveledItemGetter>()
                        .Where(l =>  StringComparer.Equals(l.EditorID, weaponEnchantmentListEditorID))
                        .Select(l => linkCache.ResolveContext<ILeveledItem, ILeveledItemGetter>(l.FormKey))
                        .Select(c => c.GetOrAddAsOverride(mod))
                        .FirstOrDefault();
                
                weaponEnchantmentList ??= new LeveledItem(mod.GetNextFormKey(), SkyrimRelease.SkyrimSE) {
                    ChanceNone = 0,
                    EditorID = weaponEnchantmentListEditorID
                };

                weaponEnchantmentList.Entries ??= new ExtendedList<LeveledItemEntry>();
                if (weaponEnchantmentList.Entries.Select(e => e.Data)
                    .NotNull()
                    .All(d => d.Reference.FormKey != sublist.FormKey)) {
                    weaponEnchantmentList.Entries.Add(new LeveledItemEntry {
                        Data = new LeveledItemEntryData {
                            Count = 1,
                            Level = 1,
                            Reference = new FormLink<IItemGetter>(sublist)
                        }
                    });
                }

                AddLeveledList<TMod, TModGetter>(mod, weaponEnchantmentList);
                
                
                var weaponTypeListEditorID = $"{prefix}ListEnch{sublist.EditorID.TrimWeaponTypeList(prefix, enchantingEditorId)}Weapon{enchantingEditorId}";
                var weaponTypeList = 
                    leveledItemGroup.Records
                        .FirstOrDefault(leveledItem => StringComparer.Equals(leveledItem.EditorID, weaponTypeListEditorID))
                 ?? linkCache.AllIdentifiers<ILeveledItemGetter>()
                        .Where(l => StringComparer.Equals(l.EditorID, weaponTypeListEditorID))
                        .Select(l => linkCache.ResolveContext<ILeveledItem, ILeveledItemGetter>(l.FormKey))
                        .Select(c => c.GetOrAddAsOverride(mod))
                        .FirstOrDefault();

                weaponTypeList ??= new LeveledItem(mod.GetNextFormKey(), SkyrimRelease.SkyrimSE) {
                    ChanceNone = 0,
                    EditorID = weaponTypeListEditorID
                };

                weaponTypeList.Entries ??= new ExtendedList<LeveledItemEntry>();
                if (weaponTypeList.Entries.Select(e => e.Data)
                    .NotNull()
                    .All(d => d.Reference.FormKey != sublist.FormKey)) {
                    weaponTypeList.Entries.Add(new LeveledItemEntry {
                        Data = new LeveledItemEntryData {
                            Count = 1,
                            Level = 1,
                            Reference = new FormLink<IItemGetter>(sublist)
                        }
                    });
                }

                AddLeveledList<TMod, TModGetter>(mod, weaponTypeList);
                break;
        }
    }
    
    private static readonly string[] WeaponTypes = {"Battleaxe", "Bow", "Dagger", "Greatsword", "Mace", "Sword", "WarAxe", "Warhammer", "Club" };
    private static readonly char[] Digits = Enumerable.Range(0, 10).Select(i => i.ToString()[0]).ToArray();
    
    private static string TrimWeaponTypeList(this string toTrim, string prefix, string enchantingEditorId) {
        //SublistEnchElvenSwordSoulTrap => Elven
        //EnchElvenSwordSoulTrap01 => Elven
        
        toTrim = toTrim
            .TrimStart(prefix)
            .TrimStart("Sublist")
            .TrimStart("Ench")
            .TrimEnd(Digits)
            .TrimEnd(enchantingEditorId);

        return WeaponTypes.Aggregate(toTrim, (current, weaponType) => current.TrimEnd(weaponType));
    }

    private static string TrimWeaponEnchantmentList(this string toTrim, string prefix, string enchantingEditorId) {
        //SublistEnchElvenSwordSoulTrap => EnchElvenSword
        //EnchElvenSwordSoulTrap01 => EnchElvenSword
        
        toTrim = toTrim
            .TrimStart(prefix)
            .TrimStart("Sublist")
            .TrimStart("Ench")
            .TrimEnd(Digits)
            .TrimEnd(enchantingEditorId);

        return toTrim;
    }
}
