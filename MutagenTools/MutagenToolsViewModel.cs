﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Skyrim;
using MutagenLibrary.Extension;
using MutagenLibrary.WPF.UtilityTypes;
using MutagenTools.Core;
using Noggog;
using Noggog.WPF;
using ReactiveUI.Fody.Helpers;
namespace MutagenTools;

public class MutagenToolsViewModel : ViewModel {
    [Reactive] public bool IsLoading { get; set; }
    [Reactive] public string LoadText { get; set; } = string.Empty;

    protected async void StartAsyncCall(Func<Task> function, string loadText = "") {
        IsLoading = true;
        LoadText = loadText;
        await function.Invoke().ConfigureAwait(false);
        IsLoading = false;
    }
    
    protected async Task<TReturn> StartAsyncCall<TReturn>(Func<Task<TReturn>> function, string loadText = "") {
        IsLoading = true;
        LoadText = loadText;
        var value = await function.Invoke().ConfigureAwait(false);
        IsLoading = false;

        return value;
    }

    public void UpdateRecords(ModKey? modKey, ObservableCollection<RecordItem> list) {
        var mod = SharedVariables.Environment.ResolveMod(modKey);
        if (mod == null) return;

        list.Clear();
        StartAsyncCall(async () => await Task.Run(() => {
            var x = (from record in mod.EnumerateMajorRecords()
                where record is not IPlacedGetter && record is not ICellGetter && record is not INavigationMeshGetter && record is not ILandscapeGetter
                 && record is not IWorldspaceGetter && record.FormKey.ModKey == mod.ModKey
                select new RecordItem(record)).ToList();
            list.AddRange(x);
        }).ConfigureAwait(false), "Collecting Records");
    }

}
