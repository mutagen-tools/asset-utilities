﻿using System.IO;
using System.Windows;
using Mutagen.Bethesda;
using Mutagen.Bethesda.Environments.DI;
using Mutagen.Bethesda.Plugins.Order.DI;
using ReactiveUI;
namespace MutagenTools;

public partial class MainWindow {
    public MainWindow() {
        var pathProvider = new PluginListingsPathProvider(new GameReleaseInjection(GameRelease.SkyrimSE));
        if (!File.Exists(pathProvider.Path)) MessageBox.Show($"Make sure {pathProvider.Path} exists.");

        App.UpdateTheme(this);
        InitializeComponent();

        DataContext = ViewModel = new AppViewModel();

        this.WhenActivated(disposable => {
            this.BindCommand(ViewModel, viewModel => viewModel.OutputFolder, view => view.OutputFolderMenuItem);
        });
    }
}