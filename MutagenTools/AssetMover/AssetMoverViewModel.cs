﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using MutagenLibrary.Assets;
using MutagenLibrary.Assets.Managers;
using MutagenLibrary.WPF.UtilityTypes;
using MutagenTools.Core;
using MutagenTools.Logging;
using Noggog;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
namespace MutagenTools.AssetMover; 

public class AssetMoverViewModel : MutagenToolsViewModel {
    private static readonly List<AssetType> BlacklistedAssetTypes = new() {
        AssetType.None, 
        AssetType.Script,
        // AssetType.Voice
    };
    
    [Reactive] public DirectoryItem? SelectedDirectory { get; set; }
    [Reactive] public string SelectedDirectoryPath { get; set; } = string.Empty;
    
    public ObservableCollection<DirectoryItem> ReferenceDirectories { get; set; } = new();
    public ObservableCollection<AssetItem> Assets { get; set; } = new();
    public ObservableCollection<object> SelectedAssets { get; set; } = new();
    public ObservableCollection<DiffItem> AssetsDiffs { get; set; } = new();

    public ICommand UpdateFilesCommand { get; }
    public ICommand MoveFilesCommand { get; }
    public ICommand AddDiffCommand { get; }

    public AssetMoverViewModel() {
        UpdateFilesCommand = ReactiveCommand.Create(UpdateFiles);
        MoveFilesCommand = ReactiveCommand.CreateFromTask(MoveFilesAsync);
        AddDiffCommand = ReactiveCommand.Create(AddDiff);
         
         this.WhenAnyValue(x => x.SelectedDirectory)
            .Throttle(TimeSpan.FromMilliseconds(250), RxApp.MainThreadScheduler)
            .Subscribe(_ => {
                UpdateFilesCommand.Execute(Unit.Default);

                SelectedDirectoryPath = SelectedDirectory?.Path ?? string.Empty;
            });
    }
    
    private async Task MoveFilesAsync() {
        IsLoading = true;
        
        var dataDir = SelectedDirectory?.Path;
        if (dataDir == null) {
            Log.Error("No data directory selected");
            return;
        }
        
        await Task.Run(() => {
            var retargets = new List<(string, string)>();

            //Move files
            LoadText = "Moving Files";
            foreach (var assetsDiff in AssetsDiffs) {
                var oldFile = Path.Combine(dataDir, assetsDiff.Old);
                var newFile = Path.Combine(dataDir, assetsDiff.New);
                if (oldFile == newFile || !File.Exists(oldFile)) continue;

                File.Move(oldFile, newFile, true);
                retargets.Add((assetsDiff.Old, assetsDiff.New));
            }
            
            //Replace mod references
            var selectedMods = SharedVariables.LoadOrder.Where(mod => mod.IsSelected).Select(mod => mod.ModKey).ToHashSet();
            foreach (var mod in SharedVariables.Environment.LoadOrder) {
                if (!selectedMods.Contains(mod.Key)) continue;
                LoadText = $"Replacing Mod References: {mod.Key.FileName}";
                if (mod.Value.Mod != null) MoveEngine.RetargetModReferences(mod.Value.Mod, new PluginManager(SharedVariables.Environment, mod.Key.FileName), retargets);
            }
            
            //Replace asset references
            LoadText = "Replacing Asset References";
            foreach (var referenceDirectory in ReferenceDirectories) {
                MoveEngine.RetargetDirectoryReferences(referenceDirectory.Path, retargets);
            }
        }).ConfigureAwait(false);

        AssetsDiffs.Clear();
        UpdateFilesCommand.Execute(Unit.Default);
        
        IsLoading = false;
    }
    
    private void AddDiff() {
        foreach (var asset in SelectedAssets.Cast<AssetItem>()) {
            if (AssetsDiffs.Any(diff => diff.Old == asset.Path)) return;
            AssetsDiffs.Add(new DiffItem(asset.Path, asset.Path));
        }
    }
    
    private void UpdateFiles() {
        var dataDirectory = SelectedDirectory?.Path;
        if (dataDirectory == null) return;
        
        StartAsyncCall(async () => {
            await Task.Run(() => {
                Assets.Clear();

                var dirQueue = new Queue<string>();
                dirQueue.Enqueue(dataDirectory);
                while (dirQueue.Count > 0) {
                    var dir = dirQueue.Dequeue();
                
                    Assets.AddRange(Directory.GetFiles(dir)
                        .Where(file => !BlacklistedAssetTypes.Contains(AssetTypeLibrary.GetAssetType(file)))
                        .Select(file => new AssetItem(Path.GetRelativePath(dataDirectory, file)))
                    );
                
                    foreach (var newDir in Directory.GetDirectories(dir)) {
                        dirQueue.Enqueue(newDir);
                    }
                }
            
                AssetsDiffs.Clear();
            }).ConfigureAwait(false);
            
            AssetsDiffs.Clear();
        }, "Updating Files");
    }
}
