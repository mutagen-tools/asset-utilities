﻿using System.Collections.Generic;
using System.Reactive;
using System.Reactive.Disposables;
using System.Windows.Input;
using MutagenLibrary.WPF.CustomElements.DataGrids;
using Noggog.WPF;
using ReactiveUI;
namespace MutagenTools.AssetMover;

public class AssetMoverViewBase : NoggogUserControl<AssetMoverViewModel> {}

public partial class AssetMover {
    public AssetMover() {
        InitializeComponent();

        DataContext = ViewModel = new AssetMoverViewModel();

        var addToDiff = new CommandBinding { Command = ItemView.GenericCommand };
        addToDiff.Executed += (_, _) => ViewModel.AddDiffCommand.Execute(Unit.Default);
        addToDiff.CanExecute += AssetView.CanExecuteOneOrMultipleItemsSelected;
        AssetView.AddCommands(new List<CustomCommand> {
            new("Add", addToDiff)
        });

        this.WhenActivated(disposable => {
            this.Bind(ViewModel, x => x.SelectedDirectory, x => x.DataDirectory.SelectedDirectory)
                .DisposeWith(disposable);
            
            this.Bind(ViewModel, x => x.SelectedAssets, x => x.AssetView.DataGrid.SelectedItems)
                .DisposeWith(disposable);
        });
    }
}
