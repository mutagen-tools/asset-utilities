﻿using System.Collections.Generic;
using Mutagen.Bethesda.Plugins.Aspects;
namespace MutagenTools.TextReplacer; 

public class Name : Replacer<INamed, INamedGetter> {
    public override string ReplacerName => "Name";
    
    protected override IEnumerable<string?> GetText(INamedGetter record) {
        yield return record.Name;
    }
    
    protected override void ReplaceText(INamed record, string oldText, string newText) {
        if (oldText.Equals(record.Name, TextReplacerViewModel.StringComparison)) {
            record.Name = newText;
        }
    }
}
