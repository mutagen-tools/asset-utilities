﻿using System.Reactive.Disposables;
using Noggog.WPF;
using ReactiveUI;
namespace MutagenTools.TextReplacer;

public class TextReplacerViewBase : NoggogUserControl<TextReplacerViewModel> {}

public partial class TextReplacer {
    public TextReplacer() {
        InitializeComponent();

        DataContext = ViewModel = new TextReplacerViewModel();
        
        this.WhenActivated(disposable => {
            //Diffs View
            this.Bind(ViewModel, x => x.SelectedDiffs, x => x.DiffsView.DataGrid.SelectedItems)
                .DisposeWith(disposable);
        });
    }
}