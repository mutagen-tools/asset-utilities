﻿using System.Collections.Generic;
using Mutagen.Bethesda.Skyrim;
using Mutagen.Bethesda.Strings;
namespace MutagenTools.TextReplacer; 

public class LoadScreen : Replacer<ILoadScreen, ILoadScreenGetter> {
    public override string ReplacerName => "LoadScreen";
    
    protected override IEnumerable<string?> GetText(ILoadScreenGetter record) {
        yield return record.Description.String;
    }
    protected override void ReplaceText(ILoadScreen record, string oldText, string newText) {
        if (oldText.Equals(record.Description.String, TextReplacerViewModel.StringComparison)) record.Description = new TranslatedString(TranslatedString.DefaultLanguage, newText);
    }
}
