﻿using System.Collections.Generic;
using System.Linq;
using Mutagen.Bethesda.Skyrim;
using Mutagen.Bethesda.Strings;
namespace MutagenTools.TextReplacer; 

public class Topic : Replacer<IDialogTopic, IDialogTopicGetter> {
    public override string ReplacerName => "Topic";

    protected override IEnumerable<string?> GetText(IDialogTopicGetter record) {
        yield return record.Name?.String;
        
        foreach (var dialog in record.Responses) {
            yield return dialog.Prompt?.String;
            
            foreach (var response in dialog.Responses) yield return response.Text.String;
        }
    }

    protected override void ReplaceText(IDialogTopic record, string oldText, string newText) {
        if (oldText.Equals(record.Name?.String, TextReplacerViewModel.StringComparison)) record.Name = new TranslatedString(TranslatedString.DefaultLanguage, newText);

        foreach (var resp in record.Responses) {
            if (oldText.Equals(resp.Prompt?.String, TextReplacerViewModel.StringComparison)) resp.Prompt = new TranslatedString(TranslatedString.DefaultLanguage, newText);
            
            foreach (var response in resp.Responses.Where(response => oldText.Equals(response.Text.String, TextReplacerViewModel.StringComparison))) {
                response.Text = new TranslatedString(TranslatedString.DefaultLanguage, newText);
            }
        }
    }
}
