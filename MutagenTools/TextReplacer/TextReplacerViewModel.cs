﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using DynamicData;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Plugins.Records;
using Mutagen.Bethesda.Skyrim;
using MutagenLibrary.Extension;
using MutagenLibrary.WPF.UtilityTypes;
using MutagenTools.Core;
using Noggog;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Application = System.Windows.Application;
namespace MutagenTools.TextReplacer;

public class ReplacerItem : ISelectable {
    public ReplacerItem(IReplacer replacer, bool isSelected) {
        Replacer = replacer;
        IsSelected = isSelected;
    }
    
    public IReplacer Replacer { get; }
    public bool IsSelected { get; set; }
}

public class TextReplacerViewModel : MutagenToolsViewModel {
    public static readonly SkyrimMod Output = new(new ModKey("TextReplacement", ModType.Plugin), SkyrimRelease.SkyrimSE);
    public static StringComparison StringComparison { get; private set; } = StringComparison.OrdinalIgnoreCase;
    
    public ObservableCollection<DiffItem> TextDiffs { get; set; } = new();
    public ObservableCollection<object> SelectedDiffs { get; set; } = new();
    
    public ObservableCollection<ReplacerItem> Replacements { get; set; } = new();

    [Reactive] public bool CaseSensitive { get; set; }
    public string OldText { get; set; } = string.Empty;
    public string NewText { get; set; } = string.Empty;

    public ICommand SearchCommand { get; }
    public ICommand ReplaceAllCommand { get; }
    public ICommand DiffReplaceCommand { get; }
    public ICommand SaveCommand { get; }

    public TextReplacerViewModel() {
        foreach (var replacer in typeof(IReplacer).GetSubclassesOf().Select(c => (IReplacer?) System.Activator.CreateInstance(c)).NotNull()) {
            Replacements.Add(new ReplacerItem(replacer, true));
        }

        SearchCommand = ReactiveCommand.CreateFromTask(SearchAsync);
        ReplaceAllCommand = ReactiveCommand.CreateFromTask(ReplaceAllAsync);
        DiffReplaceCommand = ReactiveCommand.CreateFromTask(ReplaceSelectedAsync);
        SaveCommand = ReactiveCommand.CreateFromTask(SaveAsync);

        this.WhenAnyValue(x => x.CaseSensitive)
            .Subscribe(_ => StringComparison = CaseSensitive ? StringComparison.Ordinal : StringComparison.OrdinalIgnoreCase);
    }
    
    private async Task SearchAsync() {
        var selectedMods = SharedVariables.LoadOrder.Where(mod => mod.IsSelected).Select(mod => mod.ModKey).ToHashSet();

        IsLoading = true;

        TextDiffs.Clear();
        foreach (var selectedMod in SharedVariables.Environment.ResolveMods(selectedMods)) {
            var diffs = new List<DiffItem>();
            await Task.Run(() => {
                var textReferences = GetTextReferences(selectedMod, OldText, NewText);
                foreach (var (oldString, newString, record) in textReferences) {
                    var diffItem = new DiffItem(oldString, newString, record, false) { 
                        IsReadOnly = false
                    };
                    diffs.Add(diffItem);
                }
            }).ConfigureAwait(false);
            ListExt.AddRange(TextDiffs, diffs);
        }

        IsLoading = false;
    }

    private IEnumerable<(string, string, IMajorRecordGetter)> GetTextReferences(IModGetter mod, string reference, string replacement) {
        if (string.IsNullOrEmpty(reference)) yield break;

        foreach (var replacer in Replacements.Where(r => r.IsSelected).Select(r => r.Replacer)) {
            LoadText = $"Searching Text in {mod.ModKey} - {replacer.ReplacerName}";
            foreach (var (text, record) in replacer.GetTextReference(mod, reference)) {
                yield return (text, text.Replace(reference, replacement, StringComparison), record);
            }
        }
    }
    
    private async Task ReplaceAllAsync() {
        IsLoading = true;
        
        await Task.Run(() => {
            var items = new List<DiffItem>(TextDiffs);
            
            var i = 1;
            foreach (var diff in items) {
                LoadText = $"Replacing Text ({i++}/{items.Count})";
                if (diff.Data != null) ReplaceTextReferences((IMajorRecordGetter) diff.Data, diff.Old, diff.New);
            }
        }).ConfigureAwait(false);
        
        TextDiffs.Clear();

        IsLoading = false;
    }

    private async Task ReplaceSelectedAsync() {
        LoadText = "Replacing Text";
        IsLoading = true;
        
        await Task.Run(() => {
            var i = 0;
            foreach (var diff in SelectedDiffs.Cast<DiffItem>()) {
                i++;
                if (diff == null) continue;

                LoadText = $"Replacing Text ({i}/{SelectedDiffs.Count})";
                if (diff.Data != null) ReplaceTextReferences((IMajorRecordGetter) diff.Data, diff.Old, diff.New);
            }
        }).ConfigureAwait(false);

        Application.Current.Dispatcher.Invoke(() => TextDiffs.RemoveMany(SelectedDiffs.Cast<DiffItem>()));

        IsLoading = false;
    }

    private void ReplaceTextReferences(IMajorRecordQueryableGetter record, string oldText, string newText) {
        foreach (var replacer in Replacements.Where(r => r.IsSelected).Select(r => r.Replacer)) {
            replacer.ReplaceTextReference(record, oldText, newText);
        }
    }
    
    private async Task SaveAsync() {
        LoadText = "Save";

        IsLoading = true;
        await Task.Run(() => MoveEngine.WriteMod(Output)).ConfigureAwait(false);
        IsLoading = false;
    }
}
