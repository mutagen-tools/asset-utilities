﻿using System.Collections.Generic;
using Mutagen.Bethesda.Plugins.Records;
namespace MutagenTools.TextReplacer; 

public class EditorID : Replacer<IMajorRecord, IMajorRecordGetter> {
    public override string ReplacerName => "EditorID";

    protected override IEnumerable<string?> GetText(IMajorRecordGetter record) {
        yield return record.EditorID;
    }

    protected override void ReplaceText(IMajorRecord record, string oldText, string newText) {
        if (oldText.Equals(record.EditorID, TextReplacerViewModel.StringComparison)) {
            record.EditorID = newText;
        }
    }
}
