﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mutagen.Bethesda.Plugins.Records;
using Mutagen.Bethesda.Skyrim;
using MutagenTools.Core;
using Noggog;
namespace MutagenTools.TextReplacer; 

public interface IReplacer {
    public string ReplacerName { get; }
    
    public IEnumerable<(string, IMajorRecordGetter)> GetTextReference(IMajorRecordGetterEnumerable mod, string reference);
    public IEnumerable<string?> GetTextReference(IMajorRecordQueryableGetter record);
    public void ReplaceTextReference(IMajorRecordQueryableGetter record, string oldText, string newText);
}


public abstract class Replacer<TMajor, TMajorGetter> : IReplacer
    where TMajor : class, IMajorRecordQueryable, TMajorGetter
    where TMajorGetter : class, IMajorRecordQueryableGetter {
    public abstract string ReplacerName { get; }

    public IEnumerable<(string, IMajorRecordGetter)> GetTextReference(IMajorRecordGetterEnumerable mod, string reference) {
        return (from record in mod.EnumerateMajorRecords<TMajorGetter>()
            from text in GetTextReference(record).NotNull().Where(text => text.Contains(reference, TextReplacerViewModel.StringComparison))
            select (text, (IMajorRecordGetter) record)).ToList();
    }
    
    public IEnumerable<string?> GetTextReference(IMajorRecordQueryableGetter record) {
        return record is TMajorGetter majorGetter ? GetText(majorGetter) : Array.Empty<string?>();
    }
    
    public void ReplaceTextReference(IMajorRecordQueryableGetter record, string oldText, string newText) {
        if (record is TMajorGetter && SharedVariables.Environment.LinkCache.TryResolveContext<TMajor, TMajorGetter>(((IMajorRecordIdentifier) record).FormKey, out var context)) {
            var overrideRecord = context.GetOrAddAsOverride(TextReplacerViewModel.Output);
            if (record is IDialogTopicGetter topic) {
                foreach (var response in topic.Responses) {
                    var resp = SharedVariables.Environment.LinkCache.ResolveContext<IDialogResponses, IDialogResponsesGetter>(response.FormKey);
                    resp.GetOrAddAsOverride(TextReplacerViewModel.Output);
                }
            }
            ReplaceText(overrideRecord, oldText, newText);
        }
    }

    protected abstract IEnumerable<string?> GetText(TMajorGetter record);
    protected abstract void ReplaceText(TMajor record, string oldText, string newText);
}
