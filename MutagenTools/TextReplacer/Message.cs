﻿using System.Collections.Generic;
using Mutagen.Bethesda.Skyrim;
using Mutagen.Bethesda.Strings;
namespace MutagenTools.TextReplacer; 

public class Message : Replacer<IMessage, IMessageGetter> {
    public override string ReplacerName => "Message";

    protected override IEnumerable<string?> GetText(IMessageGetter record) {
        yield return record.Description.String;
    }

    protected override void ReplaceText(IMessage record, string oldText, string newText) {
        if (oldText.Equals(record.Description.String, TextReplacerViewModel.StringComparison)) record.Description = new TranslatedString(TranslatedString.DefaultLanguage, newText);
    }
}
