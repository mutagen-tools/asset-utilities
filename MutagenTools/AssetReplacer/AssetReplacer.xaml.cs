using System.Collections.Generic;
using System.Reactive;
using System.Reactive.Disposables;
using System.Windows.Input;
using MutagenLibrary.WPF.CustomElements.DataGrids;
using Noggog.WPF;
using ReactiveUI;
namespace MutagenTools.AssetReplacer; 

public class AssetReplacerViewBase : NoggogUserControl<AssetReplacerViewModel> {}

public partial class AssetReplacer {
    public AssetReplacer() {
        InitializeComponent();

        DataContext = ViewModel = new AssetReplacerViewModel();
        
        var addToDiff = new CommandBinding { Command = ItemView.GenericCommand };
        addToDiff.Executed += (_, _) => ViewModel.AddDiffCommand.Execute(Unit.Default);
        addToDiff.CanExecute += AssetView.CanExecuteOneOrMultipleItemsSelected;
        AssetView.AddCommands(new List<CustomCommand> {
            new("Add", addToDiff)
        });

        this.WhenActivated(disposable => {
            this.Bind(ViewModel, x => x.SelectedDirectory, x => x.DataDirectory.SelectedDirectory)
                .DisposeWith(disposable);
            
            this.Bind(ViewModel, x => x.SelectedAssets, x => x.AssetView.DataGrid.SelectedItems)
                .DisposeWith(disposable);
        });
    }
}