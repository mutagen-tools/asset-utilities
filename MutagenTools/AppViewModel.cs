using System.Windows.Input;
using MutagenTools.Core;
using Noggog.WPF;
using Syncfusion.Windows.Shared;
namespace MutagenTools;

public class AppViewModel : ViewModel {
    public ICommand OutputFolder { get; } = new DelegateCommand(OpenOutputFolder);
    
    private static void OpenOutputFolder(object obj) => MoveEngine.OpenOutFolder();
}