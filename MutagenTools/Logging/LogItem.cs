﻿using System.Windows.Media;
namespace MutagenTools.Logging;

public record LogItem(string Text, Brush Color);
