using System.Collections.Generic;
using System.Reactive.Disposables;
using System.Windows.Controls;
using Noggog;
using Noggog.WPF;
using ReactiveUI;
using Syncfusion.UI.Xaml.Grid;
namespace MutagenTools.SpellConverter;

public class SpellConverterViewBase : NoggogUserControl<SpellConverterViewModel> {}

public partial class SpellConverter {
    public SpellConverter() {
        InitializeComponent();

        DataContext = ViewModel = new SpellConverterViewModel();
        
        //Add commands
        RecordView.AddCommands(new List<MenuItem> {
            new() { Header = "Make Scroll", Command = ViewModel.MakeScrollCommand },
            new() { Header = "Make Staff", Command = ViewModel.MakeLeftHandSpellCommand },
            new() { Header = "Make Right Hand Spell", Command = ViewModel.MakeLeftHandSpellCommand },
            new() { Header = "Make Left Hand Spell", Command = ViewModel.MakeLeftHandSpellCommand },
        });
        
        //Add columns
        RecordView.DataGrid.Columns.AddRange(new[] {
            new GridCheckBoxColumn { HeaderText = "Scroll Available", MappingName = "Scroll" },
            new GridCheckBoxColumn { HeaderText = "Staff Available", MappingName = "Staff" },
            new GridCheckBoxColumn { HeaderText = "Right Hand Spell Available", MappingName = "RightHand" },
            new GridCheckBoxColumn { HeaderText = "Left Hand Spell Available", MappingName = "LeftHand" }
        });

        this.WhenActivated(disposable => {
            //Record View
            this.Bind(ViewModel, x => x.SpellRecords, x => x.RecordView.DataGrid.ItemsSource)
                .DisposeWith(disposable);
            this.Bind(ViewModel, x => x.SelectedRecords, x => x.RecordView.DataGrid.SelectedItems)
                .DisposeWith(disposable);
            
            //Source Plugin
            this.Bind(ViewModel, x => x.SourceMod, x => x.SourcePlugin.SelectedModItem)
                .DisposeWith(disposable);
        });
    }
}