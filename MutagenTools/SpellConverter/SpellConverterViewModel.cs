﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using DynamicData.Binding;
using Mutagen.Bethesda.FormKeys.SkyrimSE;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Skyrim;
using MutagenLibrary.Extension;
using MutagenLibrary.Extension.Record;
using MutagenLibrary.WPF.UtilityTypes;
using MutagenTools.Core;
using Noggog;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
namespace MutagenTools.SpellConverter;

public class SpellConverterRecordItem : RecordItem {
    public SpellConverterRecordItem(ISpellGetter spell, bool forceHasScroll = false, bool forceHasStaff = false, bool forceHasRightHand = false, bool forceHasLeftHand = false) : base(spell) {
        if (spell.HasScroll(SharedVariables.Environment) || forceHasScroll) Scroll = true;
        if (spell.HasStaff(SharedVariables.Environment) || forceHasStaff) Staff = true;
        if (spell.HasRightHand(SharedVariables.Environment) || forceHasRightHand) RightHand = true;
        if (spell.HasLeftHand(SharedVariables.Environment) || forceHasLeftHand) LeftHand = true;
    }
    
    [Reactive] public bool Scroll { get; set; }
    [Reactive] public bool Staff { get; set; }
    [Reactive] public bool RightHand { get; set; }
    [Reactive] public bool LeftHand { get; set; }
}

public class SpellConverterViewModel : MutagenToolsViewModel {
    private static readonly SkyrimMod Mod = new(new ModKey("ConvertedSpells", ModType.Plugin), SkyrimRelease.SkyrimSE);

    public ObservableCollection<SpellConverterRecordItem> SpellRecords { get; set; } = new();
    public ObservableCollection<object> SelectedRecords { get; set; } = new();

    [Reactive] public ModItem? SourceMod { get; set; }
    [Reactive] public bool LoadSingleHandSpells { get; set; }
    public string Prefix { get; set; } = string.Empty;
    
    private readonly HashSet<FormKey> _convertedScrolls = new();
    private readonly HashSet<FormKey> _convertedStaffs = new();
    private readonly HashSet<FormKey> _convertedRightHand = new();
    private readonly HashSet<FormKey> _convertedLeftHand = new();

    public ICommand UpdateRecordsCommand { get; }
    public ICommand MakeScrollCommand { get; }
    public ICommand MakeStaffCommand { get; }
    public ICommand MakeRightHandSpellCommand { get; }
    public ICommand MakeLeftHandSpellCommand { get; }

    public SpellConverterViewModel() {
        var observableSelectedRecords = SelectedRecords.ToObservableChangeSet();
        
        UpdateRecordsCommand = ReactiveCommand.Create(UpdateRecords);

        MakeScrollCommand = ReactiveCommand.CreateFromTask(
            canExecute: observableSelectedRecords
                .Select(_ => SelectedRecords.Cast<SpellConverterRecordItem>()
                    .Any(record => !record.Scroll)),
            execute: async () => {
                await ConvertSpellAsync(record => {
                    if (record.Record is not ISpellGetter spell) return;
                    if (_convertedScrolls.Contains(spell.FormKey)) return;

                    var scroll = spell.MakeScroll(SharedVariables.Environment, Mod);
                    Mod.Scrolls.Add(scroll);
                    _convertedScrolls.Add(spell.FormKey);
            
                    record.Scroll = true;
                }).ConfigureAwait(false);
            }
        );
        
        MakeStaffCommand = ReactiveCommand.Create(
            canExecute: observableSelectedRecords
                .Select(_ => SelectedRecords.Cast<SpellConverterRecordItem>()
                    .Any(record => !record.Staff)),
            execute: async () => {
                await ConvertSpellAsync(record => {
                    if (record.Record is not ISpellGetter spell) return;
                    if (_convertedStaffs.Contains(spell.FormKey)) return;
            
                    var staff = spell.MakeStaff(SharedVariables.Environment , Mod, Prefix);
                    Mod.Weapons.Add(staff);
                    _convertedStaffs.Add(spell.FormKey);
            
                    record.Staff = true;
                }).ConfigureAwait(false);
            }
        );
        
        MakeRightHandSpellCommand = ReactiveCommand.Create(
            canExecute: observableSelectedRecords
                .Select(_ => SelectedRecords.Cast<SpellConverterRecordItem>()
                    .Any(record => !record.RightHand)),
            execute: async () => {
                await ConvertSpellAsync(record => {
                    if (record.Record is not ISpellGetter spell) return;
                    if (_convertedRightHand.Contains(spell.FormKey)) return;
            
                    var staff = spell.MakeRightHandVariant(Mod);
                    Mod.Spells.Add(staff);
                    _convertedRightHand.Add(spell.FormKey);
            
                    record.RightHand = true;
                }).ConfigureAwait(false);
            }
        );
        
        MakeLeftHandSpellCommand = ReactiveCommand.Create(
            canExecute: observableSelectedRecords
                .Select(_ => SelectedRecords.Cast<SpellConverterRecordItem>()
                    .Any(record => !record.LeftHand)),
            execute: async () => {
                await ConvertSpellAsync(record => {
                    if (record.Record is not ISpellGetter spell) return;
                    if (_convertedLeftHand.Contains(spell.FormKey)) return;
            
                    var staff = spell.MakeLeftHandVariant(Mod);
                    Mod.Spells.Add(staff);
                    _convertedLeftHand.Add(spell.FormKey);
            
                    record.LeftHand = true;
                }).ConfigureAwait(false);
            }
        );
        
        this.WhenAnyValue(x => x.SourceMod, x => x.LoadSingleHandSpells)
            .Throttle(TimeSpan.FromMilliseconds(250), RxApp.MainThreadScheduler)
            .Subscribe(_ => UpdateRecordsCommand.Execute(Unit.Default));
    }

    private async Task ConvertSpellAsync(Action<SpellConverterRecordItem> converter) {
        IsLoading = true;
        
        foreach (var record in SelectedRecords.Cast<SpellConverterRecordItem>()) {
            if (record.Record is not ISpellGetter spell) continue;
            LoadText = $"Converting {spell.EditorID}";
            await Task.Run(() => converter.Invoke(record)).ConfigureAwait(false);
        }

        MoveEngine.WriteMod(Mod);

        IsLoading = false;
    }
    
    private void UpdateRecords() {
        var mod = SharedVariables.Environment.ResolveMod(SourceMod?.ModKey);
        if (mod == null) return;
        
        StartAsyncCall(async () => {
            //Async get all valid spells from the selected mod
            var records = await Task.Run(() => mod.EnumerateMajorRecords<ISpellGetter>()
                .Where(record => record.EditorID != null && (LoadSingleHandSpells || record.EquipmentType.FormKey == Skyrim.EquipType.BothHands.FormKey || record.EquipmentType.FormKey == Skyrim.EquipType.EitherHand.FormKey))
                .Select(record => new SpellConverterRecordItem(
                    record, 
                    _convertedScrolls.Contains(record.FormKey),
                    _convertedStaffs.Contains(record.FormKey),
                    _convertedRightHand.Contains(record.FormKey),
                    _convertedLeftHand.Contains(record.FormKey)
                )).ToList()
            ).ConfigureAwait(false);
        
            SpellRecords.Clear();
            SpellRecords.AddRange(records);
        }, "Collecting Records");
    }
}
