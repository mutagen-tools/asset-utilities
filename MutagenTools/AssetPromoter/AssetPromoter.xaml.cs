﻿using System.Reactive.Disposables;
using Noggog.WPF;
using ReactiveUI;
namespace MutagenTools.AssetPromoter; 

public class AssetPromoterViewBase : NoggogUserControl<AssetPromoterViewModel> { }

public partial class AssetPromoter {
    public AssetPromoter() {
        InitializeComponent();

        DataContext = ViewModel = new AssetPromoterViewModel();

        this.WhenActivated(disposable => {
            //Source Dir
            this.Bind(ViewModel, x => x.SourceDirectory, x => x.SourceDirectory.SelectedDirectory)
                .DisposeWith(disposable);
            
            //Target Dir
            this.Bind(ViewModel, x => x.TargetDirectory, x => x.TargetDirectory.SelectedDirectory)
                .DisposeWith(disposable);
            
            //Source Plugin
            this.Bind(ViewModel, x => x.SourceMod, x => x.SourcePlugin.SelectedModItem)
                .DisposeWith(disposable);

            //Target Plugin
            this.Bind(ViewModel, x => x.TargetMod, x => x.TargetPlugin.SelectedModItem)
                .DisposeWith(disposable);
            this.Bind(ViewModel, x => x.TargetMods, x => x.TargetPlugin.Mods)
                .DisposeWith(disposable);
            
            //Injection Plugin
            this.Bind(ViewModel, x => x.InjectionMod, x => x.InjectionPlugin.SelectedModItem)
                .DisposeWith(disposable);
            this.Bind(ViewModel, x => x.InjectionMods, x => x.InjectionPlugin.Mods)
                .DisposeWith(disposable);
        });
    }
}