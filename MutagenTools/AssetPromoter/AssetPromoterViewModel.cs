﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Mutagen.Bethesda;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Plugins.Records;
using Mutagen.Bethesda.Skyrim;
using MutagenLibrary.Assets;
using MutagenLibrary.Assets.Managers;
using MutagenLibrary.Assets.Parser;
using MutagenLibrary.Assets.Rules;
using MutagenLibrary.Extension;
using MutagenLibrary.WPF.CustomWindows;
using MutagenLibrary.WPF.UtilityTypes;
using MutagenTools.Core;
using MutagenTools.Logging;
using Noggog;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
namespace MutagenTools.AssetPromoter; 

public class AssetPromoterViewModel : MutagenToolsViewModel {
    public ObservableCollection<AssetMode> AssetModes { get; } = new(Enum.GetValues<AssetMode>());
    public ObservableCollection<RecordItem> SourceRecords { get; set; } = new();
    
    public ObservableCollection<ModItem> TargetMods { get; } = new(SharedVariables.EnumerableLoadOrder.ToList());
    public ObservableCollection<ModItem> InjectionMods { get; } = new();

    [Reactive] public ModItem? SourceMod { get; set; }
    [Reactive] public ModItem? TargetMod { get; set; }
    [Reactive]  public ModItem? InjectionMod { get; set; }
    public AssetMode AssetMode { get; set; }
    public DirectoryItem? SourceDirectory { get; set; }
    public DirectoryItem? TargetDirectory { get; set; }
    
    public string AddPrefix { get; set; } = string.Empty;
    public string RemovePrefix { get; set; } = string.Empty;
    
    public ICommand UpdateRecordsCommand { get; }
    public ICommand MoveRecordsCommand { get; }
    public ICommand RefreshTargetPluginsCommand { get; }

    public AssetPromoterViewModel() {
        UpdateRecordsCommand = ReactiveCommand.Create(() => UpdateRecords(SourceMod?.ModKey, SourceRecords));
        MoveRecordsCommand = ReactiveCommand.CreateFromTask(MoveRecords);
        RefreshTargetPluginsCommand = ReactiveCommand.Create(UpdateInjectionMods);

        this.WhenAnyValue(x => x.SourceMod)
            .Throttle(TimeSpan.FromMilliseconds(250), RxApp.MainThreadScheduler)
            .Subscribe(_ => {
                UpdateRecordsCommand.Execute(Unit.Default);
                RefreshTargetPluginsCommand.Execute(Unit.Default);
            });
    }
    
    public async Task MoveRecords() {
        IsLoading = true;

        var sourcePlugin = SharedVariables.Environment.ResolveMod(SourceMod?.ModKey);
        if (sourcePlugin == null) {
            Log.Error("No source plugin selected");
            IsLoading = false;
            return;
        }
        
        var sourceLinkCache = sourcePlugin.ToImmutableLinkCache();
        var targetMasters = GetTargetMasters();
        var referencedRecords = (from RecordItem item in SourceRecords where item.IsSelected select item.Record).ToList();
        var referencedFormKeys = referencedRecords.Select(record => record.FormKey).ToHashSet();
        var referencingFormKeys = new HashSet<FormKey>();
        
        var referencedAssets = new HashSet<Asset>();
        var uniqueAssets = new HashSet<Asset>();

        TableWindow? tableWindow = null;
        var dataGrids = new List<(IReadOnlyList<string> columns, List<List<string>> rows)>();
        
        await Task.Run(() => {
            //Get all referenced records
            LoadText = "Searching Referenced Records";
            var recordQueue = new Queue<IMajorRecordGetter>(referencedRecords);
            while (recordQueue.Count > 0) {
                var record = recordQueue.Dequeue();
                
                foreach (var formLink in record.EnumerateFormLinks()) {
                    if (formLink.IsNull || targetMasters.Contains(formLink.FormKey.ModKey) || referencedFormKeys.Contains(formLink.FormKey)) continue;
                    if (!formLink.TryResolve<IMajorRecordGetter>(sourceLinkCache, out var formLinkRecord)) continue;
                    
                    if (formLinkRecord is IPlacedGetter or ICellGetter or INavigationMeshGetter or ILandscapeGetter or IWorldspaceGetter) {
                        Log.Error($"The selection references {formLinkRecord.FormKey} which is a part of a worldspace or cell and can't be promoted-");
                        IsLoading = false;
                        return;
                    }
                    
                    recordQueue.Enqueue(formLinkRecord);
                    referencedRecords.Add(formLinkRecord);
                    referencedFormKeys.Add(formLink.FormKey);
                }
            }

            //Get referencing form keys
            LoadText = "Searching References";
            SharedVariables.ReferenceQuery.LoadModReferences(sourcePlugin);
            foreach (var formKey in referencedRecords.SelectMany(record => SharedVariables.ReferenceQuery.GetReferences(record.FormKey, sourcePlugin)).Select(link => link.FormKey)) {
                referencingFormKeys.Add(formKey);
            }

            //Create dialog prompt window
            LoadText = "Preparing UI";
            var modificationList = new List<string>();
            var formKeyList = new List<string>();
            var editorIDList = new List<string>();
            var formTypeList = new List<string>();
            foreach (var referencedRecord in referencedRecords) {
                modificationList.Add("Replace");
                formKeyList.Add(referencedRecord.FormKey.ToString());
                editorIDList.Add(referencedRecord.EditorID ?? string.Empty);
                formTypeList.Add(referencedRecord.GetType().Name);
            }
            foreach (var referencingFormKey in referencingFormKeys) {
                modificationList.Add("Edit");
                formKeyList.Add(referencingFormKey.ToString());

                var referencingRecord = sourceLinkCache.Resolve<IMajorRecordGetter>(referencingFormKey);
                editorIDList.Add(referencingRecord.EditorID ?? string.Empty);
                formTypeList.Add(referencingRecord.GetType().Name);
            }
            dataGrids.Add((
                new[] { "Modification", "FormID", "EditorID", "FormType" },
                new List<List<string>> { modificationList, formKeyList, editorIDList, formTypeList }
            ));
            
            if (AssetMode != AssetMode.Ignore && SourceDirectory != null && TargetDirectory != null) {
                //Get referenced assets
                LoadText = "Searching Referenced Assets";
                var pluginParser = new RecordParser(SharedVariables.Environment.LinkCache);
                foreach (var record in referencedRecords) {
                    foreach (var (path, assetType) in pluginParser.ParseRecord(record)) {
                        foreach (var asset in AssetRule.ApplyAssetRules(path, assetType)) {
                            referencedAssets.Add(new Asset(asset));
                        }
                    }
                }

                //Get unique assets
                LoadText = "Searching References";
                var pluginManager = new PluginManager(SharedVariables.Environment, sourcePlugin.ModKey.FileName);
                pluginManager.ParseAssets();
                uniqueAssets = pluginManager.GetUniqueAssets(referencedFormKeys);

                //Make data grid
                LoadText = "Preparing UI";
                dataGrids.Add((
                    new[] { "Location", "Asset Mode" },
                    new List<List<string>> {
                        referencedAssets.Select(asset => asset.Path).ToList(),
                        referencedAssets.Select(path => {
                            if (!File.Exists(Path.Combine(SourceDirectory.Path, path.Path))) return "File unavailable";

                            return AssetMode == AssetMode.ForceMove || (AssetMode == AssetMode.Move && uniqueAssets.Contains(path)) ? AssetMode.Move.ToString() : AssetMode.Copy.ToString();
                        }).ToList()
                    }
                ));
            }
        }).ConfigureAwait(false);


        Application.Current.Dispatcher.Invoke(() => {
            async void RoutedEventHandler(object o, RoutedEventArgs routedEventArgs) {
                tableWindow!.DialogResult = true;
                tableWindow.Close();

                LoadText = "Moving Records";
                await Task.Run(() => MoveEngine.MoveRecords(
                    referencedRecords,
                    referencingFormKeys,
                    sourcePlugin,
                    SharedVariables.Environment.ResolveMod(TargetMod?.ModKey),
                    SharedVariables.Environment.ResolveMod(InjectionMod?.ModKey),
                    RemovePrefix,
                    AddPrefix
                )).ConfigureAwait(false);
                if (AssetMode != AssetMode.Ignore && SourceDirectory != null && TargetDirectory != null) {
                    await Task.Run(() => MoveEngine.MoveAssets(referencedAssets, AssetMode == AssetMode.ForceMove ? referencedAssets : uniqueAssets, SourceDirectory.Path, TargetDirectory.Path, true)).ConfigureAwait(false);
                }
            }
            
            tableWindow = new TableWindow("Move following records & assets", dataGrids,
                new List<(string, RoutedEventHandler)> {
                    ("Start", RoutedEventHandler),
                    // ReSharper disable once AccessToModifiedClosure
                    ("Cancel", (_, _) => tableWindow?.Close())
                });

            tableWindow.ShowDialog();
        });
        IsLoading = false;
    }

    private void UpdateInjectionMods() {
        InjectionMods.Clear();
        InjectionMods.AddRange(GetTargetMasters().Select(m => new ModItem(m)));
        
        var sourcePlugin = SharedVariables.Environment.ResolveMod(SourceMod?.ModKey);
        if (sourcePlugin == null) return;

        foreach (var master in sourcePlugin.MasterReferences) {
            if (InjectionMods.All(item => item.ModKey != master.Master)) InjectionMods.Add(new ModItem(master.Master));
        }
        
        if (InjectionMods.All(item => item.ModKey != sourcePlugin.ModKey)) InjectionMods.Add(new ModItem(sourcePlugin.ModKey));
    }

    /// <summary>
    /// If no existing target plugin is selected, it will return the masters of the source plugin.
    /// Otherwise it will return the masters of the target plugin.
    /// </summary>
    /// <returns>Masters that the target plugin will have</returns>
    private HashSet<ModKey> GetTargetMasters() {
        var targetPlugin = SharedVariables.Environment.ResolveMod(TargetMod?.ModKey);
        if (targetPlugin != null) return targetPlugin.MasterReferences.Select(master => master.Master).ToHashSet();
        
        var sourcePlugin = SharedVariables.Environment.ResolveMod(SourceMod?.ModKey);
        return sourcePlugin == null ? new HashSet<ModKey>() : sourcePlugin.MasterReferences.Select(master => master.Master).ToHashSet();
    }
}
