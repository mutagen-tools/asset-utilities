﻿using System;
namespace MutagenTools.RecordReplacer; 

public partial class RecordSelectionWindow {
    public RecordSelectionWindow(IDisposable dataContext) {
        InitializeComponent();

        DataContext = dataContext;
    }
}