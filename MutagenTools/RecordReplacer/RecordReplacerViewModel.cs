﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using DynamicData.Binding;
using Microsoft.Win32;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Plugins.Records;
using MutagenLibrary.WPF.UtilityTypes;
using MutagenTools.Core;
using MutagenTools.Logging;
using Noggog;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Syncfusion.Data.Extensions;
namespace MutagenTools.RecordReplacer;

public class RecordReplacerViewModel : MutagenToolsViewModel {
    private const string Separator = " ";
    public ObservableCollection<DiffItem> RecordDiffs { get; set; } = new();
    public ObservableCollection<object> SelectedDiffs { get; set; } = new();
    
    public ObservableCollection<RecordItem> Records { get; set; } = new();
    public ObservableCollection<object> SelectedRecords { get; set; } = new();
    
    [Reactive] public ModItem? SourceMod { get; set; }

    public ICommand UpdateRecordsCommand { get; }
    public ICommand ReplaceRecordsCommand { get; }
    public ICommand AddDiffCommand { get; }
    public ICommand ImportDiffsDialogCommand { get; }
    public ICommand DiffReplaceCommand { get; }

    public RecordReplacerViewModel() {
        UpdateRecordsCommand = ReactiveCommand.Create(() => UpdateRecords(SourceMod?.ModKey, Records));
        ReplaceRecordsCommand = ReactiveCommand.Create(ReplaceRecords);
        AddDiffCommand = ReactiveCommand.Create(AddDiff);
        ImportDiffsDialogCommand = ReactiveCommand.Create(execute: async () => {
            var lines = OpenDiffFileDialog();
            await ImportDiffsAsync(lines).ConfigureAwait(false);
        });
        
        DiffReplaceCommand = ReactiveCommand.Create(
            //Check if all selected diffs have the same type
            canExecute: SelectedDiffs.ToObservableChangeSet()
                .Select(_ => SelectedDiffs
                    .Cast<DiffItem>()
                    .Select(diff => diff.Data?.GetType()).NotNull()
                    .Distinct()
                    .Count() == 1),
            execute: () => {
                var selectedItems = SelectedDiffs.Cast<DiffItem>().ToList();
                if (!selectedItems.Any()) return;

                //Show form selection window
                var majorRecordGetter = (IMajorRecordGetter?) selectedItems[0].Data;
                if (majorRecordGetter == null) return;

                var type = majorRecordGetter.Registration.GetterType;
                var selectionVm = new RecordSelectionViewModel { FormTypes = type.AsEnumerable().ToObservableCollection() };
                var recordSelectionWindow = new RecordSelectionWindow(selectionVm);
                recordSelectionWindow.ShowDialog();

                //Swap diffs
                if (selectionVm.ValidFormKey) {
                    var id = GetRecordIdentifier(selectionVm.SelectedFormKey, selectionVm.SelectedEditorID);

                    foreach (var selectedItem in selectedItems) {
                        selectedItem.New = id;
                        selectedItem.UpdateColor();
                    }
                }
            }
        );
        
        this.WhenAnyValue(x => x.SourceMod)
            .Throttle(TimeSpan.FromMilliseconds(250), RxApp.MainThreadScheduler)
            .Subscribe(_ => UpdateRecordsCommand.Execute(Unit.Default));
    }
    
    private void AddDiff() {
        foreach (var record in SelectedRecords.Cast<RecordItem>()) {
            if (RecordDiffs.Any(diff => diff.Old == record.FormID)) return;

            var id = GetRecordIdentifier(record);
            RecordDiffs.Add(new DiffItem(id, id, record.Record));
        }
    }
    
    private static string GetRecordIdentifier(RecordItem record) {
        return GetRecordIdentifier(record.FormID, record.EditorID);
    }

    private static string GetRecordIdentifier(FormKey formKey, string? editorID) {
        return GetRecordIdentifier(formKey.ToString(), editorID);
    }
    
    private static string GetRecordIdentifier(string formKey, string? editorID) {
        return formKey + (string.IsNullOrEmpty(editorID) ? string.Empty : $"{Separator}{editorID}");
    }
    
    private void ReplaceRecords() {
        StartAsyncCall(async () => {
            await Task.Run(() => {
                var retargets = RecordDiffs
                    .ToDictionary(diff => FormKey.Factory(diff.Old.Split(Separator)[0]), diff => FormKey.Factory(diff.New.Split(Separator)[0]));
            
                //Replace mod references
                var selectedMods = SharedVariables.LoadOrder.Where(mod => mod.IsSelected).Select(mod => mod.ModKey).ToHashSet();
                foreach (var mod in SharedVariables.Environment.LoadOrder) {
                    if (!selectedMods.Contains(mod.Key) || mod.Value.Mod == null) continue;

                    LoadText = $"Replacing Mod References: {mod.Key.FileName}";
                    MoveEngine.ReplaceRecordReferences(mod.Value.Mod, retargets);
                }
            }).ConfigureAwait(false);

            RecordDiffs.Clear();
        });
    }

    private List<string> OpenDiffFileDialog() {
        const string filter = "*.txt;*.csv";
        var fileDialog = new OpenFileDialog {
            Multiselect = false,
            Filter = $"Text({filter})|{filter}"
        };

        return fileDialog.ShowDialog() is true && File.Exists(fileDialog.FileName) ? File.ReadLines(fileDialog.FileName).ToList() : new List<string>();
    }
    
    private async Task ImportDiffsAsync(IReadOnlyList<string> readLines) {
        const char separator = ',';

        IsLoading = true;
        await Task.Run(() => {
            for (var i = 0; i < readLines.Count; i++) {
                var formKeys = readLines[i].Split(separator);
                if (formKeys.Length > 2) Log.Error($"Line {i} has too many form ids");
                
                LoadText = $"Importing {formKeys[0]}";
                
                if (!SharedVariables.LinkCache.TryResolve(FormKey.Factory(formKeys[0]), out var record)) {
                    Log.Error($"{formKeys[0]} is not available in the loaded environment");
                    return;
                }
                
                var oldRecordID = GetRecordIdentifier(formKeys[0], record.EditorID);
                var newRecordID = oldRecordID;
                if (formKeys.Length > 1) {
                    if (!SharedVariables.LinkCache.TryResolve(FormKey.Factory(formKeys[1]), out var newRecord)) {
                        Log.Error($"{formKeys[1]} is not available in the loaded environment");
                        return;
                    }
                    
                    newRecordID = GetRecordIdentifier(formKeys[1], newRecord.EditorID);
                }
                
                Application.Current.Dispatcher.Invoke(() => RecordDiffs.Add(new DiffItem(GetRecordIdentifier(formKeys[0], record.EditorID),  newRecordID, record)));
            }
        }).ConfigureAwait(false);
        IsLoading = false;
    }
}
