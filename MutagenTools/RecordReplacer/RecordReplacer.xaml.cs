﻿using System.Collections.Generic;
using System.Reactive;
using System.Reactive.Disposables;
using System.Windows.Input;
using MutagenLibrary.WPF.CustomElements.DataGrids;
using Noggog.WPF;
using ReactiveUI;
namespace MutagenTools.RecordReplacer; 

public class RecordReplacerViewBase : NoggogUserControl<RecordReplacerViewModel> {}

public partial class RecordReplacer {
    public RecordReplacer() {
        InitializeComponent();

        DataContext = ViewModel = new RecordReplacerViewModel();
        
        var addToDiff = new CommandBinding { Command = ItemView.GenericCommand };
        addToDiff.Executed += (_, _) => ViewModel.AddDiffCommand.Execute(Unit.Default);
        addToDiff.CanExecute += RecordView.CanExecuteOneOrMultipleItemsSelected;
        RecordView.AddCommands(new List<CustomCommand> {
            new("Add", addToDiff)
        });

        this.WhenActivated(disposable => {
            //Source Plugin
            this.Bind(ViewModel, x => x.SourceMod, x => x.SourcePlugin.SelectedModItem)
                .DisposeWith(disposable);
            
            //Record View
            this.Bind(ViewModel, x => x.SelectedRecords, x => x.RecordView.DataGrid.SelectedItems)
                .DisposeWith(disposable);
            
            //Diffs View
            this.Bind(ViewModel, x => x.SelectedDiffs, x => x.DiffsView.DataGrid.SelectedItems)
                .DisposeWith(disposable);
        });
    }
}