﻿using System;
using System.Collections.ObjectModel;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Skyrim;
using Noggog.WPF;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
namespace MutagenTools.RecordReplacer;

public class RecordSelectionViewModel : ViewModel {
    public RecordSelectionViewModel() {
        this.WhenAnyValue(v => v.SelectedFormKey)
            .Subscribe(_ => {
                ValidFormKey = SelectedFormKey != FormKey.Null;
            });
    }

    [Reactive]
    public FormKey SelectedFormKey { get; set; } = FormKey.Null;
    [Reactive]
    public string? SelectedEditorID { get; set; } = string.Empty;
    [Reactive]
    public bool ValidFormKey { get; set; }

    [Reactive]
    public ObservableCollection<Type> FormTypes { get; set; } = new() { typeof(IFactionGetter) };
}
