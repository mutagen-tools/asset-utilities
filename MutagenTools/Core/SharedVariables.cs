﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Mutagen.Bethesda;
using Mutagen.Bethesda.Environments;
using Mutagen.Bethesda.Plugins.Cache;
using Mutagen.Bethesda.Skyrim;
using MutagenLibrary.ReferenceCache;
using MutagenLibrary.WPF.UtilityTypes;
namespace MutagenTools.Core; 

public class SharedVariables {
    public static  ILinkCache<ISkyrimMod, ISkyrimModGetter> OnlyIdentifiersLinkCache { get; set; } = GameEnvironment.Typical.Skyrim(SkyrimRelease.SkyrimSE, LinkCachePreferences.OnlyIdentifiers()).LinkCache;
    
    public static readonly IGameEnvironment<ISkyrimMod, ISkyrimModGetter> Environment = GameEnvironment.Typical.Skyrim(SkyrimRelease.SkyrimSE);
    public static  ILinkCache<ISkyrimMod, ISkyrimModGetter> LinkCache { get; set; } = Environment.LinkCache;
    public static ObservableCollection<ModItem> LoadOrder { get; set; } = new(Environment.LoadOrder.ListedOrder.Select(mod => new ModItem(mod.ModKey)));
    public static IEnumerable<ModItem> EnumerableLoadOrder { get; set; } = Environment.LoadOrder.ListedOrder.Select(mod => new ModItem(mod.ModKey));
    public static readonly ReferenceQuery ReferenceQuery = new();
}