﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Mutagen.Bethesda;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Plugins.Records;
using Mutagen.Bethesda.Skyrim;
using MutagenLibrary.Assets;
using MutagenLibrary.Assets.Managers;
using MutagenLibrary.Assets.Replacer;
using MutagenLibrary.ReferenceCache;
namespace MutagenTools.Core;

public enum AssetMode {
    Ignore,
    Copy,
    Move,
    ForceMove
}

public static class MoveEngine {
    private static readonly string OutFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Output");
    private const string OutFilePrefix = "Modified";
    private const string PatchFilePrefix = "InjectedFrom";
    private static readonly ReferenceQuery ReferenceQuery = new();
    private static readonly Random Random = new();
    
    public static void MoveRecords(List<IMajorRecordGetter> referencedRecords, HashSet<FormKey> referencingFormKeys,
        ISkyrimModGetter sourcePlugin, ISkyrimModGetter? targetPlugin = null, ISkyrimModGetter? injectionTarget = null, string removePrefix = "", string addPrefix = "") {
        if (referencedRecords.Count == 0) return;
        
        var formKeysMappings = new Dictionary<FormKey, FormKey>();
        var sourceOverride = new SkyrimMod(new ModKey($"{OutFilePrefix}{sourcePlugin.ModKey.Name}", ModType.Plugin), SkyrimRelease.SkyrimSE);

        //Create new target mod if necessary
        SkyrimMod outputTargetMod;
        if (targetPlugin == null) {
            outputTargetMod = new SkyrimMod(new ModKey($"{PatchFilePrefix}{sourcePlugin.ModKey.Name}", ModType.Plugin), SkyrimRelease.SkyrimSE);
            foreach (var modKey in sourcePlugin.MasterReferences.Select(master => master.Master)) {
                outputTargetMod.ModHeader.MasterReferences.Add(new MasterReference { Master = modKey });
            }
        } else {
            outputTargetMod = new SkyrimMod($"{OutFilePrefix}{targetPlugin.ModKey}", SkyrimRelease.SkyrimSE);
            outputTargetMod.DeepCopyIn(targetPlugin);
        }

        var freeFormKeys = new HashSet<uint>();
        if (injectionTarget != null) {
            var range = (int) Math.Pow(16, 6);
            var bitmap = uint.MaxValue - (uint) range;
            freeFormKeys = Enumerable.Range(0, range).Select(i => (uint) i).ToHashSet();
            
            foreach (var identifier in injectionTarget.ToImmutableLinkCache().AllIdentifiers()) {
                freeFormKeys.Remove(identifier.FormKey.ID & bitmap);
            }
        }

        //Loop through source mod
        var sourceLinkCache = sourcePlugin.ToImmutableLinkCache();
        foreach (var referencedRecord in referencedRecords.Select(record => sourceLinkCache.ResolveContext<IMajorRecord, IMajorRecordGetter>(record.FormKey))) {
            //Duplicate to new plugin - try to inject into injection target
            FormKey targetFormKey;
            if (injectionTarget != null) {
                var formKey = freeFormKeys.ElementAt(Random.Next(freeFormKeys.Count));
                freeFormKeys.Remove(formKey);
                
                targetFormKey = new FormKey(injectionTarget.ModKey, formKey);
                var duplicate = referencedRecord.Record.Duplicate(targetFormKey);
                
                if (duplicate.EditorID != null) duplicate.EditorID = CalculateEditorID(duplicate.EditorID, removePrefix, addPrefix);
                outputTargetMod.GetTopLevelGroup((duplicate as IMajorRecordGetter).Registration.GetterType).AddUntyped(duplicate);
            } else {
                var duplicate = referencedRecord.DuplicateIntoAsNewRecord(outputTargetMod, referencedRecord.Record.EditorID);
                if (duplicate.EditorID != null) duplicate.EditorID = CalculateEditorID(duplicate.EditorID, removePrefix, addPrefix);
                
                targetFormKey = duplicate.FormKey;
            }
            
            //Remove entry in override plugin
            var overrideRecord = referencedRecord.GetOrAddAsOverride(sourceOverride);
            overrideRecord.MajorRecordFlagsRaw |= (int) SkyrimMajorRecord.SkyrimMajorRecordFlag.Deleted;
            
            //Populate form key mapping
            formKeysMappings.Add(referencedRecord.Record.FormKey, targetFormKey);
        }
        
        //Replace links of records that are referenced the old records
        foreach (var formKey in referencingFormKeys) {
            if (!sourceLinkCache.TryResolveContext<IMajorRecord, IMajorRecordGetter>(formKey, out var referencingContext)) continue;

            var overrideRecord = referencingContext.GetOrAddAsOverride(sourceOverride);
            overrideRecord.RemapLinks(formKeysMappings);
        }
        
        //Replace links in target plugin
        outputTargetMod.RemapLinks(formKeysMappings);
        
        //Write plugins
        WriteMod(sourceOverride);
        WriteMod(outputTargetMod);
    }

    public static void WriteMod(IModGetter mod) {
        var fileInfo = new FileInfo(Path.Combine(OutFolder, mod.ModKey.FileName));
        if (!fileInfo.Exists) fileInfo.Directory?.Create();
        mod.WriteToBinaryParallel(fileInfo.FullName);
    }

    private static string CalculateEditorID(string editorID, string removePrefix = "", string addPrefix = "") {
        if (editorID.StartsWith(removePrefix)) editorID = editorID[removePrefix.Length..];
        
        return addPrefix + editorID;
    }

    /// <summary>
    /// Move or copy assets from the source directory to the target directory.
    /// </summary>
    /// <param name="copyAssets">These assets will be copied to targetDirectory.</param>
    /// <param name="moveAssets">These assets will be moved to targetDirectory (not present in sourceDirectory anymore).</param>
    /// <param name="sourceDirectory">Paths of the assets start with sourceDirectory base path. This is where assets will be searched for.</param>
    /// <param name="targetDirectory">This is the base path where assets will be copied/moved to.</param>
    /// <param name="overwrite">Copy/Move operation will overwrite existing files in targetDirectory</param>
    public static void MoveAssets(HashSet<Asset> copyAssets, HashSet<Asset> moveAssets, string sourceDirectory, string targetDirectory, bool overwrite) {
        //Move and copy assets
        foreach (var asset in moveAssets) {
            copyAssets.Remove(asset);
            var path = asset.Path;
            var srcPath = Path.Combine(sourceDirectory, path);
            var dstPath = Path.Combine(targetDirectory, path);
            
            if (File.Exists(srcPath)) {
                new FileInfo(dstPath).Directory?.Create();
                File.Move(srcPath, dstPath, overwrite);
            }
        }

        foreach (var path in copyAssets.Select(asset => asset.Path)) {
            var srcPath = Path.Combine(sourceDirectory, path);
            var dstPath = Path.Combine(targetDirectory, path);
            
            if (File.Exists(srcPath)) {
                new FileInfo(dstPath).Directory?.Create();
                File.Copy(srcPath, dstPath, overwrite);
            }
        }
    }

    public static void RetargetModReferences(ISkyrimModGetter mod, PluginManager modPluginManager, List<(string, string)> retargets) {
        var linkCache = mod.ToImmutableLinkCache();
        modPluginManager.ParseAssets();
        
        var outputTargetMod = new SkyrimMod(new ModKey($"{OutFilePrefix}{mod.ModKey.Name}", ModType.Plugin), SkyrimRelease.SkyrimSE);
        foreach (var (oldFile, newFile) in retargets) {
            foreach (var (formKey, _) in modPluginManager.GetUsages(new Asset(oldFile), AssetTypeLibrary.GetAssetType(oldFile))) {
                var context = linkCache.ResolveContext<IMajorRecord, IMajorRecordGetter>(FormKey.Factory(formKey));
                
                var overrideRecord = context.GetOrAddAsOverride(outputTargetMod);
                MutagenLibrary.Assets.Replacer.RecordReplacer.ReplaceAssets(linkCache, overrideRecord, oldFile, newFile);
            }
        }
            
        if (outputTargetMod.GetRecordCount() > 0) {
            var outputFileInfo = new FileInfo(Path.Combine(OutFolder, outputTargetMod.ModKey.FileName));
            if (!outputFileInfo.Exists) outputFileInfo.Directory?.Create();
            outputTargetMod.WriteToBinaryParallel(outputFileInfo.FullName);
        }
    }
    
    public static void RetargetDirectoryReferences(string directory, List<(string, string)> retargets) {
        var nifManager = new NifAssetManager(directory, false);
        nifManager.ParseAssets();
        
        foreach (var (oldFile, newFile) in retargets) {
            foreach (var usage in nifManager.GetUsages(new Asset(oldFile))) {
                if (!File.Exists(usage)) continue;
                
                NifReplacer.ReplaceAssets(usage, oldFile, newFile);
            }
        }
    }

    public static void ReplaceRecordReferences(ISkyrimModGetter mod, Dictionary<FormKey, FormKey> replacements, SkyrimMod? output =  null) {
        output ??= new SkyrimMod(new ModKey($"{OutFilePrefix}{mod.ModKey.Name}", ModType.Plugin), SkyrimRelease.SkyrimSE);

        var linkCache = mod.ToImmutableLinkCache();
        foreach (var usageFormLink in replacements.Keys.SelectMany(formKey => ReferenceQuery.GetReferences(formKey, mod))) {
            if (linkCache.TryResolveContext<IMajorRecord, IMajorRecordGetter>(usageFormLink.FormKey, out var context)) {
                context.GetOrAddAsOverride(output);
            }
        }
        
        output.RemapLinks(replacements);

        if (output.GetRecordCount() > 0) {
            var targetFileInfo = new FileInfo(Path.Combine(OutFolder, output.ModKey.FileName));
            if (!targetFileInfo.Exists) targetFileInfo.Directory?.Create();
            output.WriteToBinaryParallel(targetFileInfo.FullName);
        }
    }

    public static void OpenOutFolder() {
        Directory.CreateDirectory(OutFolder);
        
        using var process = new Process {
            StartInfo = new ProcessStartInfo {
                FileName = "explorer",
                Arguments = $"\"{OutFolder}\""
            }
        };
        process.Start();
    }
}